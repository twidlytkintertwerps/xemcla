#!/usr/bin/env python3
"""Tkinter GUI interface for the XEMCLA project/program.

# LICENSE

Copyright (c) [2018] [Casper Peters and Johan Schneiders]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""
__author__ = "Casper Peters & Johan Schneiders"
__copyright__ = "MIT"
__credits__ = ["Casper Peters", "Johan Schneiders"]
__version__ = "1.0"
__maintainer__ = "Casper Peters & Johan Schneiders"
__status__ = "Prototype"
# imports
from multiprocessing import freeze_support
from tkinter import *
from tkinter import ttk

from xemcla_resources import icon as xr_icon
from xemcla_resources.widgets.misc import global_tkutils
from xemcla_resources.widgets.moarwidgets.status_bar import StatusBar
from xemcla_resources.widgets.tabs import preprocessing, clustering, view


# classes


class ApplicationInterface(Tk):
    """Main window of application."""
    def __init__(self):
        """Constructs /sets up the user interface."""
        super().__init__()
        # setup functional widgets

        # tabs
        self.tabs = ttk.Notebook(self)
        self.prep_tab = preprocessing.PreprocessingTab(self.tabs, self)
        self.cl_tab = clustering.ClusteringTab(self.tabs, self)
        self.view_tab = view.ViewTab(self.tabs, self)
        # no need to pack or add the individual tabs, this is built-in
        self.tabs.pack(fill="both", expand=True)
        self.status_bar = StatusBar(self, anchor="e")
        self.status_bar.pack(expand=False, fill="x")
        self.wait_visibility()
        # style main window
        self.configure_appearance()
        self.tabs.pack(expand=True, fill=BOTH)
        return

    def configure_appearance(self):
        """defines / applies basic styling of the application such as titles and window size"""
        self.title("XEMCLA")
        window_icon = PhotoImage(master=self, data=xr_icon.ICON)
        self.tk.call('wm', 'iconphoto', self._w, window_icon)
        global_tkutils.window_center_placement(self)
        return

# functions


def main():
    # work
    print("Started GUI...")
    root = ApplicationInterface()
    root.configure_appearance()
    root.mainloop()
    return


if __name__ == "__main__":
    freeze_support()
    sys.exit(main())
