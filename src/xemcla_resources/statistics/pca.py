#!/usr/bin/env python3
"""Module that holds functions necessary to perform pca."""
# imports
import sys
import numpy as np


# constants
SPACER = "\n" + "#"*20 + "\n"
# functions


def generate_sample_data():
    """Generates sample data."""
    rng = np.random.RandomState(1)
    X = np.dot(rng.rand(2, 2), rng.randn(2, 100)).T
    return X


def cov(matrix):
    """Calculates the covariance matrix given a matrix of data.
    (instances by row, dimensions by column)
    """
    # manual calculation
    x = matrix
    n = x.shape[0]
    j = np.ones(n).T

    colmeans = 1/n*x.T.dot(j)
    deviations = x - colmeans
    covmat = 1/(n-1) * deviations.T.dot(deviations)
    # calculation using numpy function
    # covmat = np.cov(x.T)
    return covmat


def eig(matrix):
    """Calculates eigen values and eigen vectors of given sqare matrix.
    eigen values and corresponding vectors are sorted descending.
    """
    eigen_values, eigen_vectors = np.linalg.eigh(matrix)
    # eigh returns values and vectors ascending by eigen value, flip them for convenience
    return eigen_values[::-1], eigen_vectors[:, ::-1]


def main(argv=None):
    """Initiates program."""
    if argv is None:
        argv = sys.argv
    # work
    print(__doc__)
    return 0

# initiate


if __name__ == "__main__":
    sys.exit(main())
