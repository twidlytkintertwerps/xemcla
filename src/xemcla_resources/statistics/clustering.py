#!/usr/bin/env python3
"""Module wrapping clustering functions."""
# imports
import sys

import numpy as np
from scipy.cluster.vq import kmeans2 as scvq_kmeans, ClusterError
from scipy.spatial.distance import euclidean as euclidean_distance

# constants
SPACER = "\n" + "#"*20
# classes
# functions


def calc_score_ratio(data, centroids, labels):
    """Calculates between_ss / total_ss.
    :param data: data that was clustered
    :param centroids: centroids of clustering
    :param labels: labels for data instances
    :return: between_ss / total_ss(fraction of total variance explained by the clustering)
    """
    sample_mean = data.mean(axis=0)
    total_ss = sum(euclidean_distance(sample_mean, instance) ** 2 for instance in data)

    between_ss = sum((euclidean_distance(sample_mean, centroid)) ** 2 * cluster_size
                     for centroid, cluster_size in zip(centroids, np.bincount(labels)))

    return between_ss / total_ss


def auto_kmeans(data, threshold, verbose=True, **kwargs):
    """Performs kmeans clustering, automatically optimizing the number of clusters to use.
    args:
    -data:numpy 2d array containing instances to cluster(instances by row, dimensions by column)
    -threshold: minimum value of "between_ss / total_ss" the clustering should have.
     (sum of squared distances from each centroid to the sample mean, squared distances weighted by corresponding
     number of data points)
     divided by
     (sum of squared distances from each data point to the sample mean)
     this measure describes what proportion of the total distance/variance is described/caught by the clustering.
     1 -> clustering perfectly  explains all variance.
     0 -> clustering explains no variance.
    - verbose: boolean indicating if progress should be logged to stdout
    - **kwargs: keyword arguments to pass to scipy.cluster.vq.kmeans2
    """
    # initialize variables
    n_centers = 2
    ratio = 0
    logging_template = "{}\t| {}"

    if verbose:
        print("Started clustering with auto-kmeans.")
        print(logging_template.format("centers", "between_ss / total_ss * 100"))

    while ratio < threshold:
        try:
            # execute clustering, may raise exception if one of the clusters ends up empty
            centroids, labels = scvq_kmeans(data, n_centers, missing="raise", **kwargs)
        except ClusterError:
            # try again without incrementing n_clusters
            continue
        else:
            ratio = calc_score_ratio(data, centroids, labels)
            if verbose:
                print(logging_template.format(n_centers, round(ratio * 100, 3)) + "%")
                
            n_centers += 1  
        
    if verbose:
        print("surpassed threshold of {} using {} clusters.".format(threshold, n_centers-1), "finished clustering.", sep="\n")
    return labels, centroids, ratio


def main(argv=None):
    """Initiates program."""
    if argv is None:
        argv = sys.argv
    # work
    return 0


# initiate
if __name__ == "__main__":
    sys.exit(main())
