#!/usr/bin/env python3
"""Calculates a gray-level co-occurence matrix (glcm) and statistics given a PixelBlob object."""
# imports
import sys

import numpy as np
from skimage.feature import greycomatrix, greycoprops

# constants
# all measures of a glcm + some custom ones
GREYCOPROPS_MEASURES = ["contrast", "dissimilarity", "homogeneity", "energy", "correlation", "ASM"]
TEXTURE_MEASURES = GREYCOPROPS_MEASURES + ["mean_intensity"]  # custom texture measure names go in here
# classes
# functions


def mean_intensity(glcm):
    """Calculates average pixel intensity.
    :arg glcm: a 2D np array
    """
    if not type(glcm) == np.ndarray:
        raise ValueError("expected numpy array. got {}instead".format(type(glcm).__name__))
    elif not glcm.ndim == 2:
        raise ValueError("expected 2d numpy array. gor shape {} instead.".format(glcm.shape))

    s = np.sum(glcm, axis=1)
    i = range(len(glcm))
    mean = sum(s*i) / sum(glcm.flatten())
    return mean


def make_glcm_for_obj(px_blob_obj, distances, angles, symmetric=False, normed=False):
    """Generates a grey level co-occurrence matrix for given pixel_blob object, strips last row and column (this is the
    value used for filling in gaps for the objects).

    :param px_blob_obj:
    An instance of the PixelBlob class

    FOR THE FOLLOWING PARAMETERS, PLEASE REFER TO THE 'skimage.feature.greycomatrix' DOCUMENTATION;
    :param distances:
    :param angles:
    :param symmetric:
    :param normed:
    :return:
    returns the output 'skimage.feature.greycomatrix'.
    """
    # first get a matrix that is properly fit for a glcm calculation.
    raw_matrix = px_blob_obj.get_matrix_for_glcm_calc()
    # levels is the amount of rows and columns or the amount of possible intensities.
    # here, it is defined as 257 because we'd want all the glcm's for all blobs have the same dimensions so the
    # statistics don't diverge too much. It is however possible to base it on the current pixel blob. this is specified
    # in the comment after the levels param.
    return greycomatrix(raw_matrix, distances, angles, levels=257,  # levels=np.amax(raw_matrix)+1
                        symmetric=symmetric, normed=normed)[:-1, :-1, :, :]


def calculate_glcm_stats(glcm, stat_names):
    """Calculates texture measures of given glcm.
    :arg glcm a greyscale co-occurrence matrix as produced by skimage.feature.greycomatrix
    :arg stat_names ordered collection of names of statistics to calculate.
    (see TEXTURE_MEASURES)
    :return list containing requested texture measures in order of stat_names.
    """
    stats = list()
    for name in stat_names:
        if name in GREYCOPROPS_MEASURES:
            stats.append(greycoprops(glcm, prop=name)[0, 0])
        elif name in TEXTURE_MEASURES:
            # generate custom texture measure
            # stats.append(...)
            if name == "mean_intensity":
                stats.append(mean_intensity(glcm[:, :, 0, 0]))
            pass
        else:
            raise ValueError("unknown texture-measure: " + name)
    return stats


def calculate_texture_measures(blobs_per_group, stat_names, distance, angle, symmetric=False, normed=False):
    """"Calculates texture measures for every blob using a greylevel co-occurrence matrix.
    :arg blobs_per_group: dict linking a group label to a list of PixelBlob objects.
    :arg stat_names: names of texture measures to calculate.
    :arg distance: distance to use in pixel relation for glcm.
    :arg angle: angle(radians) to use in pixel relation for glcm.
    :arg symmetric: weather to use a symmetric glcm or not.
    :arg normed: weather to use a normalized glcm or not.
    :return dictionary linking group label to 2-d np array in which every row
    holds texture measures of a single PixelBlob object.
    """
    stats_per_group = dict()
    for group, blobs in blobs_per_group.items():
        group_stats = list()
        for blob in blobs:
            # calculate texture measures
            glcm = make_glcm_for_obj(blob, [distance], [angle], symmetric=symmetric, normed=normed)
            group_stats.append(calculate_glcm_stats(glcm, stat_names))
        # generate 2-d np array
        stats_per_group[group] = np.array(group_stats)
    return stats_per_group


def main(argv=None):
    """Initiates program."""
    if argv is None:
        argv = sys.argv
    # work
    print(__doc__)
    return 0

# initiate


if __name__ == "__main__":
    sys.exit(main())
