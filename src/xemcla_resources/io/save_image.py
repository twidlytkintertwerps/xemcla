#!/usr/bin/env python3
"""This module only contains 1 function to save an image with imagio, it is defined separately to supress annoying
warnings.
"""
# imports
import os
import sys
from contextlib import contextmanager

import imageio


# constants
# classes
# functions
@contextmanager
def suppress_stdout_and_stderr():
    """Function that suppresses output of a given function, used with the 'with function():' clause."""
    with open(os.devnull, "w") as devnull:
        old_stdout = sys.stdout
        old_stderr = sys.stderr
        sys.stdout = devnull
        sys.stderr = devnull
        try:
            yield
        finally:
            sys.stdout = old_stdout
            sys.stderr = old_stderr
    return


def save_image(name, image):
    """Suppresses imageio.imsave errors for improperly scaled images."""
    try:
        with suppress_stdout_and_stderr():
            imageio.imsave(name, image)
    except:
        print("An error occurred when trying to save an image; {}".format(name))
    return


def main(argv=None):
    """Initiates program."""
    if argv is None:
        argv = sys.argv
    # work
    return 0


# initiate
if __name__ == "__main__":
    sys.exit(main())
