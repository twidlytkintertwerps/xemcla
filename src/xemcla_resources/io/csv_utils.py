#!/usr/bin/env python3
"""Module hat holds functions for reading (image) csv's and combining them into a single file."""
# imports
import sys

import numpy as np


# constants
# classes
# functions


def read_csv(filename, sep=", ", strip=",\n", **kwargs):
    """Parses given csv file and returns its content as a numpy 2d array."""
    return np.genfromtxt(strip_file_lines(filename, strip), delimiter=sep, **kwargs)

    
def strip_file_lines(filename, chars=" "):
    """Generator that iterates over all lines in given file,
    removing given chars from end of every line if present.
    """
    with open(filename) as file:
        for line in file:
            yield line.strip(chars)
            

def flatten_csv(filename, **kwargs):
    """Returns a np 1d array containing all cell values of given csv.
    -filename: path(string) to the csv file to be flattened.
    -kwargs: keyword arguments to pass to read_csv
    """
    return read_csv(filename, **kwargs).flatten()
                

def stack_csvs(*filenames, **kwargs):
    """Generator that yields a tuple containing
    cell value n from every csv file given.
    args:
    -*filenames: paths(string) to the csv files to be stacked.
      files must be comma separated(not tab!) and should all have the same
      size: the smallest file will determine StopIteration.
    -**kwargs: keyword arguments to pass to flatten_csv
    """
    # zip returns an iterator
    return zip(*(flatten_csv(filename, **kwargs) for filename in filenames))
                    

def main(argv=None):
    """Initiates program."""
    if argv is None:
        argv = sys.argv
    # work
    print(__doc__)
    return 0

# initiate


if __name__ == "__main__":
    sys.exit(main())
