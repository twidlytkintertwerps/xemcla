#!/usr/bin/env python3
"""This class is a storage class for a kmeans result in the gui, only used for being pickled for later use."""
# imports
import pickle
import sys


# constants
# classes


class KmeansResultObject:
    """This class is a storage class for a kmeans result in the gui, only used for being pickled for later use."""
    def __init__(self, image, image_labels, centroids, element_labels, score, tab_name):
        """Stores values when instance is made.
        For the following params, please refer to the 'xemcla_resources.tabs.view.register_cluster_image' function.
        :param image:
        :param image_labels:
        :param centroids:
        :param element_labels:
        :param score:
        :param tab_name:
        """
        self.image = image
        self.image_labels = image_labels
        self.centroids = centroids
        self.element_labels = element_labels
        self.score = score
        self.tabname = tab_name
        return

    def put_cucumber_in_brine(self, filename):
        """Put the class in a python pickle object, this can later be loaded in again under the view tab with the
        'xemcla_resources.tabs.view.extract_from_brine' function.
        :param filename:
        The full filename of the pickle object, it can have any extension but '.pickle' is preferred.
        """
        with open(filename, "wb") as opened:
            pickle.dump(self, opened)
        return

    def save_centroid_table_to_csv(self, filename):
        """Saves the centroidLegend as shown with a clustering in the gui, into a csv file so this can be read in with
        other programs later.
        :param filename:
        The filename the csv should have, preferably with the '.csv' file extension.
        """
        content = ["cluster"]
        for label in self.element_labels:
            content.append(label)
        content = [",".join(content)+"\n"]

        for n, centroid_set in enumerate(self.centroids, start=1):
            content.append(str(n)+","+",".join([str(centroid) for centroid in centroid_set])+"\n")

        with open(filename, "w") as opened:
            opened.write("".join(content))
        return

    def get_view_args(self):
        """Returns arguments for the 'xemcla_resources.tabs.view.register_cluster_image' function."""
        return self.image, self.image_labels, self.centroids, self.element_labels, self.score, self.tabname

# functions


def main(argv=None):
    """Initiates program."""
    if argv is None:
        argv = sys.argv
    # work
    print(__doc__)
    return 0

# initiate


if __name__ == "__main__":
    sys.exit(main())
