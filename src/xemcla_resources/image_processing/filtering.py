#!/usr/bin/env python3
"""Module for image filtering."""
# imports
import sys
from multiprocessing import Process, Manager
from os.path import basename

import numpy as np
import skimage.restoration as restoration
from scipy.cluster.vq import whiten
from skimage import filters

from ..io.csv_utils import read_csv


# constants
IMAGE_WIDTH = 1024
SPACER = "#" * 20 + "\n"
FILTERS = {"gaus": filters.gaussian,
           "tv": restoration.denoise_tv_chambolle,
           "bilateral": restoration.denoise_bilateral,
           "wavelet": restoration.denoise_wavelet,
           "non-local_means": restoration.denoise_nl_means
           }
# dict linking filtering method to possible parameters
FILTER_PARAMETERS = {
    "gaus": "sigma",
    "tv": "weight",
    "bilateral": "sigma_spatial",
    "wavelet": "sigma",
    "non-local_means": "h"
}
# classes
# functions


def flatten(np_array):
    """Flattens given numpy array by row."""
    return np_array.flatten()


def prepare_image(image, filtering_instructions):
    """Applies given filtering instructions to image.
    :param image: np 2d-array as used by skimage.
    :param filtering_instructions: ordered collection linking name of filtering method(see FILTERS)
     to a dict of parameters, as required by the corresponding function(see FILTERS)
    """
    filtered_image = image
    for name, parameters in filtering_instructions:
        if name in FILTERS:
            # apply filtering step
            if name == "bilateral":
                filtered_image = FILTERS[name](filtered_image, multichannel=False, **parameters)
            else:
                filtered_image = FILTERS[name](filtered_image, **parameters)
        else:
            raise ValueError("\"{}\" is not a valid filter name.".format(name))
    return filtered_image


def prepare_image_in_parallel(image, filtering_instructions, results, result_index):
    """Preprocesses a single image
    :param image: image to filter
    :param filtering_instructions: ordered collection linking name of filtering method(see FILTERS)
         to a dict of parameters, as required by the corresponding function(see FILTERS)
    :param results: list to put result in.
    :param result_index: index in result list to put tesult at
    """
    filtered_image = prepare_image(image, filtering_instructions)
    results[result_index] = filtered_image
    return


def preprocess_edx_data(filenames, filtering_instructions, scale=False):
    """"Preprocesses given EDX data and returns a 2d np array
    containing the chemical composition of one pixel per row
    args:
    :param filenames: <element>.csv... notice! program is written for csv's that are delimited by ","
     and in which every line ends with ",\n"
    :param filtering_instructions: dictionary linking name of filtering method(see FILTERS)
     to a tuple of paramaters, as required by the corresponding function(see FILTERS)
     :param scale: boolean indicating if prepped data should be scaled(z-scores)
    :return
        -2-d np-array describing chemical composition of single pixel per row,
        -list of colnames for array
        -dimensions of image
    """
    elements = [basename(file).split(".")[0] for file in filenames]

    # apply specified preprocessing to images, order of prepped images corresponds to order of elements
    # preprocessing is done in parallel
    processes = list()
    prepped_images = Manager().list([None for _ in filenames])
    # set up processes
    for index in range(len(filenames)):
        image = read_csv(filenames[index], sep=",")
        p = Process(target=prepare_image_in_parallel, args=(image, filtering_instructions, prepped_images, index))
        processes.append(p)
        p.start()
    # wait for all processes to finish
    for process in processes:
        process.join()
    # stack images to matrix in which every image is represented in a single column
    stacked_images = np.array(list(zip(*(map(flatten, prepped_images)))))

    if scale:
        stacked_images = whiten(stacked_images)

    return stacked_images, elements, prepped_images[0].shape


def main(argv=None):
    """Initiates program."""
    if argv is None:
        argv = sys.argv
    # work
    print(__doc__)
    return 0

# initiate


if __name__ == "__main__":
    sys.exit(main())
