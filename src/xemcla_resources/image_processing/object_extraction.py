#!/usr/bin/env python3
"""Function for object extraction from kmeans clustering based on cluster labels,
extracts data as seperate objects and make a list for each kmeans label.
"""
# imports
import sys

import numpy as np

from .pixel_blob import PixelBlob


# constants
# classes
# functions


def get_objects(data, data_clustered, minsize=3, only_extract=None):
    """See main script doc for explanation.
    Params;
    :param data:
    data is the original electron microscopy image with 2 channels (monochrome)
    :param data_clustered:
    data_clustered is the labeled kmeans result in form of an image, also monochrome
    :param minsize:
    minsize is the minimum mount of pixels an objects should contain. If a pixel object is too small, it is discarded.
    :param only_extract:
    only_extract should be a list of labels that need to be extracted, any others will be skipped. Default (None),
    behaviour makes it so that all clusters get extracted.
    :return:
    returns a dictionary (dict) with;
        :param key:
        A cluster label (int)
        :param value:
        A list of PixelBlob objects (list)
    """
    # copy kmeans data into a new variable and convert it to int16 so it can be used for keeping track of different
    # states in extraction. (-1 = visited, -2 = pending neighbour)
    data_flagged = data_clustered.copy().astype("int16")

    # if only extract is set, set all other clusters that are not taken into account to -1.
    if only_extract:
        try:
            iter(only_extract)
            # get all possible k-groups
            k_groups = np.unique(data_flagged).tolist()
            for k_value in k_groups:
                # check if k value is wanted
                if k_value in only_extract:
                    continue
                else:
                    # if it is not, set the "already visited" flag.
                    data_flagged[data_flagged == k_value] = -1
        except:
            print("Warning, \"only_extract\" is not iterable and values cannot be skipped!")

    # make some storage variables and start out loop.
    objects = {}
    xrange = range(0, data_clustered.shape[1])
    yrange = range(0, data_clustered.shape[0])
    # first add all possible coordinates to the notdone list
    for x in xrange:
        for y in yrange:
            # first check if not already looped over the current pixel by looking at the flagged data
            if data_flagged[(y, x)] != -1:
                # now make an object out of the current pixel and make it swipe itself
                raw_pixel_object = PixelBlob((x, y), data, data_clustered, data_flagged)
                data_flagged = raw_pixel_object.swipe()
                # save variables from extraction in single blob for keeping track.

                # uncomment the following line to save a representation of a blob;
                # raw_pixel_object.save_projection(my_directory)

                # check if key already exists and add object to its group
                # empty out image data from the pixel object since it won't be used after it has been swiped.
                raw_pixel_object.wipe_image_data()
                if len(raw_pixel_object.pixels) >= minsize:
                    try:
                        objects[raw_pixel_object.type].append(raw_pixel_object)
                    except KeyError:
                        objects[raw_pixel_object.type] = [raw_pixel_object]
    return objects


def main(argv=None):
    """Initiates program."""
    if argv is None:
        argv = sys.argv
    # work
    print(__doc__)
    return 0

# initiate


if __name__ == "__main__":
    sys.exit(main())
