#!/usr/bin/env python3
"""This module contains some handy calculation functions for coordinate handling (used mostly by pixel_blob)."""
# imports
import math
import sys


# constants
# classes
# functions


def get_px_range(px_gap):
    """Calculate the offsets needed to calculate neighbours with a certain "px_gap" (pixel gap) which represents how
    much extra radius you want for looking up neighbour pixels. if 0 is specified, a 3x3 area is selected, if 1 is
    specified, 5x5, and so on...
    :param px_gap:
    pixel gap (integer)
    :return:
    list of offsets (list of integers)
    """
    neighbour_reach = px_gap + 2
    # make negative indices
    neg = range(-1, -neighbour_reach, -1)
    # make positive indices
    pos = range(1, neighbour_reach, 1)
    # add them all together and return
    return (*neg, 0, *pos)


def get_neighbour_pxls(xy, px_gap, xmax, ymax):
    """Calculates the neighbouring pixels given certain information
    :param xy:
    tuple of integers describing the x and y coordinate of starting pixel that will be the centre.
    :param px_gap:
    pixel gap --> see get_px_range
    :param xmax:
    xmax, the maximum x that is possible in the coordinate system (int)
    :param ymax:
    ymax, the maximum y that is possible in the coordinate system (int)
    :return:
    list with coordinates tuple; (x,y), excluding the given coordinate. (so only the neighbours)
    """
    # unpack x and y for easier access
    x = xy[0]
    y = xy[1]
    xmin = 0
    ymin = 0
    # calculate offsets that will define the reach you have to nearest neighbours
    neighbour_reaches = get_px_range(px_gap)
    # get the coordinates
    coords = [(x + a, y + b) for a in neighbour_reaches for b in neighbour_reaches]
    # pop the current coordinate, is not a neighbour but is self.
    coords.pop(coords.index((x, y)))
    # filter the coordinates that are not within the scope if outer coordinates not within range. this sometimes
    # prevents an unnecessary list comprehension

    neighbour_reach_min = min(neighbour_reaches)
    neighbour_reach_max = max(neighbour_reaches)

    # even add up with + for negative values because otherwise it will add instead of subtract
    x_lower = x + neighbour_reach_min
    y_lower = y + neighbour_reach_min
    # always + max
    x_upper = x + neighbour_reach_max
    y_upper = y + neighbour_reach_max

    if x_lower < xmin or x_upper > xmax or y_lower < ymin or y_upper > ymax:
        # if xmin <= x <= self.xmax and ymin <= y <= self.ymax ===> keep coordinate.
        coords = [i for i in coords if xmin <= i[0] <= xmax and ymin <= i[1] <= ymax]

    return coords


def swap_indexes(coords):
    """Swaps indexes of coordinates xy -> yx or yx -> xy ; useful for working with numpy
    :param coords:
    the coordinates with xy or yx
    :return:
    """
    return coords[1], coords[0]


def calculate_limits(xcoords):
    """This function will calculate the limits of a coordinate list, so the minimum and maximum x's and y's.
    :param xcoords:
    this is a list of tuples with (x, y) coordinates
    :return:
    tuple of limits;
    (xmin, xmax, ymin, ymax)
    """
    xmin = math.inf
    xmax = 0
    ymin = math.inf
    ymax = 0
    # just a bunch of ifs, this does really need comments its pretty straighforward I'd say...
    for coordinate in xcoords:
        x = coordinate[0]
        y = coordinate[1]
        if xmin > x:
            xmin = x
        if ymin > y:
            ymin = y
        if xmax < x:
            xmax = x
        if ymax < y:
            ymax = y
    return xmin, xmax, ymin, ymax


def convert_to_relative_zero_matrix(xorig, yorig, xmin, ymin):
    """Calculates the relative zero matrix coordinates if the given coordinate would be projected into it's own local
    matrix.
    This function can also be used for calculating the new boundries of said matrix by plugging the xmax and ymax
    values into the xorig and yorig.
    This function is useful for extracting slices out of a matrix of which not all values are present.
    :param xorig:
    x coordinate from original matrix
    :param yorig:
    y coordinate from original matrix
    :param xmin:
    minimum x from original matrix
    :param ymin:
    minimum y from original matrix
    :return:
    new relative coordinates
    (newx, newy)
    """
    return xorig-xmin, yorig-ymin


def main(argv=None):
    """Initiates program."""
    if argv is None:
        argv = sys.argv
    # work
    print(__doc__)
    return 0

# initiate


if __name__ == "__main__":
    sys.exit(main())
