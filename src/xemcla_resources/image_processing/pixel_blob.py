#!/usr/bin/env python3
"""PixelBlob class; Object extraction from kmeans clustering, extracts data as seperate objects with same labels."""
# imports
import os
import sys
from os import path

import imageio
import numpy as np

from . import coordinate_calculations as cc


# constants
# classes


class PixelBlob:
    """PixelBlob class; Object extraction from kmeans clustering, extracts data as seperate objects with same labels."""
    def __init__(self, a_pixel, data, data_clustered, data_flagged, pixel_gap=0):
        """Initiates code, makes all neccesary variables, prepares them and starts swipe.

        :param a_pixel:
        a starting pixel - should be a tuple with xy: (x, y)
        :param data:
        See object_extraction.get_objects documentation
        :param data_clustered:
        See object_extraction.get_objects documentation
        :param data_flagged:
        See object_extraction.get_objects documentation
        :param pixel_gap:
        See coordinate_calculations.get_px_range
        """
        # keep track of master pixel coordinates.
        self.x = a_pixel[0]
        self.y = a_pixel[1]
        # keep track of visited pixels
        self.pixels = list()
        self.pixels.append((self.x, self.y))
        # get master pixels type, this will be the objects type/class
        self.type = data_clustered[(self.y, self.x)]
        # define all the matrix data / image data
        self.data = data
        self.data_clustered = data_clustered
        self.data_flagged = data_flagged
        self.data_flagged[(self.y, self.x)] = -1
        # calculate xmax and ymax for neighbour function and swipe
        self.xmax = self.data_clustered.shape[1] - 1
        self.ymax = self.data_clustered.shape[0] - 1
        # define pixel gap
        self.pixel_gap = pixel_gap
        # make and get neighbours for current master pixel
        self.pending_neighbours = []
        # get initial neighbours
        initial_neighbours = cc.get_neighbour_pxls((self.x, self.y), self.pixel_gap, self.xmax, self.ymax)
        # and check them...
        for n in initial_neighbours:
            # only add them to pending neighbours if of same type, type does not need to be checked, these will be the
            # first neighbours.
            reverse_axis = cc.swap_indexes(n)
            if self.data_clustered[reverse_axis] == self.type:
                # add to pending neighbours and set neighbour flag (-2)
                self.data_flagged[reverse_axis] = -2
                self.pending_neighbours.append(n)
        # swipe and continue until there's no neighbours left to be checked.
        self.swipe()
        # convert pixels to tuple for faster processing later on, and conserving memory.
        self.pixels = tuple(self.pixels)
        # save projection onto a blank square. This is handy for calculating grey-level co-occurrence matrices
        self.projection = self.calculate_projection()
        return

    def wipe_image_data(self):
        """Clear out used images, so as to preserve memory. The python garbage collector will clean up after this."""
        # empty out unnecesarily kept data, the image data. this is purely for swiping and saving after a swipe
        self.data_flagged = None
        self.data_clustered = None
        return

    def swipe(self):
        """Swipe function, gathers all other pixels that are part of the currently selected pixel with same k-group.
        :return:
        returns data_flagged which keeps track of visited pixels and other states.
        """
        while self.pending_neighbours:
            x, y = self.pending_neighbours.pop(0)
            flag = self.data_flagged[(y, x)]
            # check if current pixel is not flagged.
            if flag != -1:
                # if it is not, do more checks
                pixel_type = self.data_clustered[y, x]
                # test the pixel type, if same, set flag to visited (-1) and append to visited
                if self.type == pixel_type:
                    self.data_flagged[(y, x)] = -1
                    self.pixels.append((x, y))
                    # now get neighbours of the current pixel
                    neighbours_neighbour = cc.get_neighbour_pxls((x, y), self.pixel_gap, self.xmax, self.ymax)
                    for n in neighbours_neighbour:
                        # only add them to pending neighbours if of same type and if not flagged or already present
                        # (present = flag; -2).
                        reverse_axis = cc.swap_indexes(n)
                        nflag = self.data_flagged[reverse_axis]
                        if nflag != -1 and nflag != -2 and self.data_clustered[reverse_axis] == self.type:
                            # add to pending neighbours and set neighbour flag (-2)
                            self.data_flagged[reverse_axis] = -2
                            self.pending_neighbours.append(n)
        return self.data_flagged

    def calculate_projection(self):
        """Makes a local matrix for the current object for glcm calculations or for making an image of the object.
        The values of the original data will be used. The result will be a square matrix with filled value -1
        :return:
        an nparray gets return with dimensions (M,N) which will represent the blob with overload original values.
        Pixels that are not in the square are represented with filler value -1.
        """
        # use min values to scale to a reference 0,0 point later
        # get biggest values too
        xmin, xmax, ymin, ymax = cc.calculate_limits(self.pixels)

        # first make an empty matrix filled with whatever values
        xboundary, yboundary = cc.convert_to_relative_zero_matrix(xmax, ymax, xmin, ymin)
        filler = np.full(((yboundary + 1), (xboundary + 1)), -1, dtype="int16")

        # overlap original picture
        for coordinate in self.pixels:
            x = coordinate[0]
            y = coordinate[1]
            filler[cc.swap_indexes(cc.convert_to_relative_zero_matrix(x, y, xmin, ymin))] = self.data[y, x]
        return filler

    def save_projection(self, directory, bg_color=255):
        """Save the projection of the current blob to a specified output directory.
        :param directory:
        Directory of file to be saved to.
        :param bg_color:
        The color in 8-bit greyscale that will be used for the background.
        """
        # set background as white
        image = self.projection.astype("uint8")
        image[image < 0] = bg_color
        # try to make the directory if it does not exist.
        try:
            os.mkdir(directory)
        except IOError:
            print("For some reason directory ({}) could not be created, this is most likely due to it already "
                  "existing.".format(directory))
        except PermissionError:
            print("You don't have permission to create directory; ({})".format(directory))
        # save the file.
        filename = "kgroup{}_x{}_y{}".format(self.type, self.x, self.y)
        imageio.imsave(path.join(directory, filename) + ".png", image)
        return

    def get_matrix_for_glcm_calc(self):
        """Makes a matrix for easy calculation of the grey level co-occurrence matrix.
        :return:
        ndarray (M,N) that can be used for glcm calculation ('skimage.feature.greycomatrix').
        """
        # get the projection image.
        new_matrix = self.projection

        # with the following code the max value of the matrix can be defined by the max value in the original data.
        # real_vals = []
        #
        # for coordinate in self.pixels:
        #     real_vals.append(self.data[cc.swap_indexes(coordinate)])
        # max(real_vals)+1

        # overlay matrix with 256 (above the 255-color threshold) this is meant as a special value to remove later, so
        # the glcm can be calulated. To calculate a glcm, one always needs a square matrix.
        new_matrix[new_matrix < 0] = 256
        return new_matrix

# functions


def main(argv=None):
    """Initiates program."""
    if argv is None:
        argv = sys.argv
    # work
    print(__doc__)
    return 0

# initiate


if __name__ == "__main__":
    sys.exit(main())
