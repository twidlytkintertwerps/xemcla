#!/usr/bin/env python3
"""Module for transparently coloring in sections of a grayscale image."""
# imports
import sys

import numpy as np
from skimage.color import label2rgb


# functions


def mark_image(image, label_dict, **kwargs):
    """Marks/colors the original grayscale image based on given label_dict.
    Label dict is a dictionary linking a label(string or int) to a collection of coordinates  (x, y).
    **kwargs are keyword-arguments passed to label2rgb, excluding bg_label and image.
    """
    labels = np.zeros(image.shape[:2])
    # construct a 2-d label array
    for label, coordinates in enumerate(label_dict.values(), 1):
        # generate a tuple for np indexing
        coordinates = np.array(coordinates)[:, ::-1]
        index_tuple = tuple(np.moveaxis(coordinates, -1, 0))
        # set labels
        labels[index_tuple] = label
    result = label2rgb(labels, image=image, bg_label=0, **kwargs)
    return result


def main(argv=None):
    """Initiates program."""
    if argv is None:
        argv = sys.argv
    # work
    print(__doc__)
    return 0

# initiate


if __name__ == "__main__":
    sys.exit(main())