#!/usr/bin/env python3
"""A few handy dandy tools/functions for popup-windows and window placement."""
# imports
import sys
import tkinter as tk
import tkinter.filedialog as tkf
import tkinter.ttk as ttk


# constants
# classes
# functions


def window_center_placement(window, w=None, h=None, minw=None, minh=None):
    """Given a root window or popup window, set the window to the center of the detectable screen resolution.
    :param window:
    The root window or popup window to be centered
    :param w:
    The set width of the window
    :param h:
    The set height of the window
    :param minw:
    The minimum width of the window
    :param minh:
    The minimum height of the window

    The widths and heights are None by default and will be based on how tkinter packs the window instead.

    :return:
    The window with applied settings
    """
    # if w or h is none, get standard.
    if not w or not isinstance(w, int):
        w = window.winfo_width()
    if not h or not isinstance(h, int):
        h = window.winfo_height()
    # get screen width and height
    ws = window.winfo_screenwidth()  # width of the screen
    hs = window.winfo_screenheight()  # height of the screen
    # print(hs, ws)
    # calculate x and y coordinates for the Tk root window
    x = (ws / 2) - (w / 2)
    y = (hs / 2) - (h / 2)
    # set the dimensions of the screen
    # and where it is placed
    window.geometry('%dx%d+%d+%d' % (w, h, x, y))
    if minw and isinstance(minw, int) and minh and isinstance(minh, int):
        window.minsize(minw, minh)
    else:
        window.minsize(w, h)
    return window


def window_center_placement_and_size(window, w=800, h=600, minw=800, minh=600):
    """Centers window according to the window_center_placement function, same parameters apply for this function.
    Also sizes window with given defaults.
    :param window: toplevel widget.
    :param w:
    standard; 800
    :param h:
    standard; 600
    :param minw:
    standard; 800
    :param minh:
    standard; 600
    :return:
    The window with applied settings.
    """
    # first fix the windowsize
    # also set the minimums
    window.minsize(minw, minh)
    return window_center_placement(window, w, h)


def browse_files_function(textbox, **options):
    """Filedialog for selecting multiple files, files are stored as strings in a textbox.

    :param textbox:
    The textbox to be used for saving the strings of filenames in.
    :param options:
    arguments for he tkinter.filedialog.askopenfilenames
    """
    filenames = tkf.askopenfilenames(**options)
    if filenames:
        # first remove all text
        textbox.config(state=tk.NORMAL)
        textbox.delete('1.0', tk.END)
        # the put in filenames
        for filename in filenames:
            textbox.insert(tk.END, filename + "\n\n")
        # disable the textbox to prevent editing
        textbox.config(state=tk.DISABLED)
    return


def browse_dir_function(textbox):
    """Filedialog for selecting a directory, files are stored as strings in a textbox the same way as the
    browse_files_function.

    :param textbox:
    see browse_files_function
    """
    directory = tkf.askdirectory()
    if directory:
        # first remove all text
        textbox.config(state=tk.NORMAL)
        textbox.delete('1.0', tk.END)
        # then put in directory
        textbox.insert(tk.END, directory + "\n\n")
        # disable the textbox to prevent editing
        textbox.config(state=tk.DISABLED)
    return


def retrieve_files_from_textbox(textbox):
    """Retrieves filenames from a textbox and puts them in a list, this is depending on that browse_files_function is
    used.

    :param textbox:
    see browse_files_function
    :return:
    list of filename strings
    """
    files = []
    for item in textbox.get("1.0", tk.END).split("\n\n"):
        new_item = item.strip().lstrip()
        if new_item:
            files.append(new_item)
    return files


def retrieve_dir_from_textbox(textbox):
    """Retrieves directory name from a textbox and returns it, this is depending on that browse_dir_fucntion is used.

    :param textbox:
    see browse_dir_function
    :return:
    string of the given directory.
    """
    return textbox.get("1.0", tk.END).split("\n\n")[0].strip().lstrip()


def show_wrong_value_popup(desired_type):
    """Function used for making an error pop up if a value is wrong.

    :param desired_type:
    Actual type of the class that should have been defined.
    """
    # gets actual string of class
    desired_type = str(desired_type).split("\'")[1]
    # make a popup
    general_popup("Wrong value!", "You inserted an invalid value! Value needs to be of type;"
                                  "\"{}\"".format(desired_type),
                  "Ok.")
    return


def general_popup(title, description_text, button_text):
    """Makes tk.Toplevel window popup with a given title, description and button text.

    :param title:
    Text for the title
    :param description_text:
    Text for the description
    :param button_text:
    Text for the button
    """
    win = tk.Toplevel()
    win.wm_title(title)
    # gets actual string of class
    win.desc = tk.Label(win, text=description_text)
    win.desc.pack()
    win.closebutton = ttk.Button(win, text=button_text, command=win.destroy)
    win.closebutton.pack()

    win.resizable(0, 0)
    win.wait_visibility()
    win = window_center_placement(win)
    win.grab_set()
    return


def main(argv=None):
    """Initiates program."""
    if argv is None:
        argv = sys.argv
    # work
    print(__doc__)
    return 0

# initiate


if __name__ == "__main__":
    sys.exit(main())
