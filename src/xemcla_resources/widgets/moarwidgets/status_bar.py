#!/usr/bin/env python3
"""Holds the StatusBar class."""
# imports
import sys
import tkinter


# constants
# classes


class StatusBar(tkinter.Label):
    """a simple status bar"""
    def __init__(self, master, **options):
        """:param master: parent widget
        :param options: options passed to tkinter.Label
        """
        self._textvar = tkinter.StringVar()
        super().__init__(master, textvariable=self._textvar,  **options)
        return

    def display(self, message):
        """Displays given message, overriding the existing one.
        :arg message: message to display
        """
        self._textvar.set(message)
        return

    def clear(self):
        """Clears the status bar."""
        self._textvar.set("")
        return

# functions


def main(argv=None):
    """Initiates program."""
    if argv is None:
        argv = sys.argv
    # work
    print(__doc__)
    return 0

# initiate


if __name__ == "__main__":
    sys.exit(main())