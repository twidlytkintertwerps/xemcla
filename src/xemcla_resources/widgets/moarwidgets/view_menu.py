#! /usr/bin/env python3
"""Module holds the ViewMenu class and its subclasses."""
# imports
import time
from itertools import chain
from tkinter import Frame, Label, Button
from tkinter.filedialog import asksaveasfilename

from scipy.cluster.vq import kmeans2 as scvq_kmeans
from skimage import img_as_ubyte
from skimage.color import label2rgb

from xemcla_resources.image_processing.object_extraction import get_objects
from xemcla_resources.io.save_image import save_image
from xemcla_resources.statistics.glcm_calc import *
from .buttonned_scale import ButtonedScale
from .centroid_legend import CentroidLegend
from .color_picking import ColorMenu, hex_to_rgb
from .numbered_buttons import NumberedButtons


# classes


class ImageMenu(Frame):
    """Minimal image menu."""
    def_extension = ".png"  # default extension for saving images

    def __init__(self, master, root, controller, display, image, name, **options):
        """Menu linked to single image."""
        super().__init__(master, **options)
        self._root = root
        self._controller = controller
        self._display = display
        self._image = image
        self._name = name

        label = Label(self, text=name)
        label.pack(padx=10, pady=10)

        save_button = Button(self, text="save image", command=self.save_image)
        save_button.pack()
        return

    def save_image(self):
        """Saves the image associated with the menu, generates a popup."""
        filename = asksaveasfilename(initialfile=self._name + ImageMenu.def_extension,
                                     defaultextension=ImageMenu.def_extension,
                                     filetypes=[("png", ".png"), ("all", "*")])
        # if save not cancelled by user
        if filename:
            save_image(filename, self._image)
        return


class ClusterImageMenu(ImageMenu):
    """Image menu implementing functionality for cluster images."""
    def __init__(self, master, root, controller, display, em_image, label_image, centers, coordinate_labels, score,
                 name):
        """:param master: parent widget
        :param display: ImageDisplay on which image is displayed
        :param em_image: original image; np array
        :param label_image: cluster image: np array, int
        :param centers: array with centroid coordinates
        :param coordinate_labels: axis labels of centroid coordinates
        :param score: score of given clustering
        :param name: name of displayed image
        """
        super().__init__(master, root, controller, display, em_image, name)
        # set up attributes
        self._labels = np.unique(label_image)
        self._label_image = label_image
        self._em_image = em_image
        self._centers = centers
        self._score = score

        # set up widgets
        self._color_picker = ColorMenu(self, len(self._labels),
                                       numbers=True,
                                       text="select colors for cluster labels",
                                       bd=0)
        self._color_picker.pack(pady=20)
        self._color_picker.bind("<<ColorChange>>", self._update_color_scheme)

        self._opacity_slider = ButtonedScale(self, bd=3,
                                             relief="ridge",
                                             from_=0, to=1, resolution=0.01,
                                             text="Label opacity:",
                                             command=self._update_color_scheme)
        self._opacity_slider.pack(pady=20)
        self._opacity_slider.set(0.15)

        if centers:
            # legend with centroid locations
            self._legend = CentroidLegend(self, centers, coordinate_labels)
            self._legend.pack(expand=True, fill="x")
        if score is not None:
            l = Label(self, text="This clustering explains {:.2f}% of total variance".format(score*100),
                      wraplength=100, justify="left")
            l.pack(expand=True, fill="both")
        # "split cluster" buttons
        self._split_buttons = NumberedButtons(self, len(self._labels), command=self._split_cluster,
                                              text="select cluster to split on texture", start=1)
        self._split_buttons.pack(pady=10)

        return

    def _update_color_scheme(self, event=None, transparent="#ffffff"):
        """Highlights clusters on the displayed image
        :param event: dummy argument, catches tkinter event.
        :param transparent: color to consider "transparent". All clusters with this color will not be highlighted.
        """
        # set transparent to rgb
        transparent = hex_to_rgb(transparent)
        # update color scheme / highlight self.image
        colors = list(map(hex_to_rgb, self._color_picker.get()))
        opacity = self._opacity_slider.get()

        transparent_clusters = [i for i, color in enumerate(colors) if color == transparent]

        self._image = img_as_ubyte(label2rgb(self._label_image, image=self._em_image, colors=colors, alpha=opacity))
        # make the corresponding groups transparent if it is the set transparent color.
        for k_value in transparent_clusters:
            transparent_pixels = self._label_image == k_value
            self._image[transparent_pixels, :] = self._em_image[transparent_pixels, np.newaxis]

        self._display.update_image(self._name, self._image)
        return

    def get_colors(self):
        return self._color_picker.get()

    def split_blob_group(self, em_image, label_image, target):
        """Splits required kmeans goup(target) in two, based on mean intensity of blobs.
        :param em_image: em image(np 2d array)
        :param label_image: 2d np array giving labels (positive integers) for every pixel in the em image.
        :param target: cluster (int) to split.
        :return: new label image in which given cluster is split.
        """
        start_time = time.time()
        # extract blobs per group: {group: [blob...]}
        # update status bar
        self._root.status_bar.display("Clustering blob texture features...")
        self.update()
        print("Extracting blobs")
        blobs = get_objects(em_image, label_image, minsize=40, only_extract=[target])
        print("Calculating stats")
        stats_per_group = calculate_texture_measures(blobs, TEXTURE_MEASURES, 1, 0, symmetric=False, normed=True)
        # split k-means group target in two clusters
        print("Calculating new clusters")
        centroids, labels = scvq_kmeans(stats_per_group[target][:, 0], 2, missing="raise")
        # create 2new groups
        original_group = blobs[target]
        group_a = [blob for i, blob in enumerate(original_group) if labels[i]]  # labels contains 0 and 1
        group_b = [blob for i, blob in enumerate(original_group) if not labels[i]]
        blobs[target] = group_a
        blobs[label_image.max() + 1] = group_b

        # reconstruct image based on blobs
        labels = label_image.copy()
        # construct a 2-d label array
        for blobs_value, blob_list in blobs.items():
            # generate a tuple for np indexing
            coordinates = list(chain(*[blob.pixels for blob in blob_list]))
            coordinates = np.array(coordinates)[:, ::-1]
            index_tuple = tuple(np.moveaxis(coordinates, -1, 0))
            # set labels
            labels[index_tuple] = blobs_value
        print("Blob-clustering done!")
        final_time = time.time()-start_time
        print("Blob-clustering took {:.2f}s".format(final_time))
        # update status bar
        self._root.status_bar.display("Done blob-clustering! took; {:.2f}s".format(final_time))
        self.update()
        return labels

    def _split_cluster(self, target):
        """Splits requested cluster in half, based on the texture of the em image
        :arg target: cluster to split(int)
        """
        target -= 1
        print("---\nStarted to split on texture!")
        print("Labels in current image:", np.unique(self._label_image))
        print("Splitting cluster {}".format(target))
        split_image = self.split_blob_group(self._em_image, self._label_image, target)
        print("New clusters:", np.unique(split_image))
        # self, em_image, label_image, centers, coordinate_labels, score, name
        self._controller.register_cluster_image(self._em_image, split_image, None, None, None, self._name +
                                                "_split{}".format(target+1))
        return

# functions


def main(argv=None):
    """Initiates program."""
    if argv is None:
        argv = sys.argv
    # work
    print(__doc__)
    return 0

# initiate


if __name__ == "__main__":
    sys.exit(main())
