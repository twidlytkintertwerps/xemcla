#!/usr/bin/env python3
"""Holds tkinter widgets for picking a color."""
# imports
import sys
from tkinter import Label, LabelFrame
from tkinter.colorchooser import askcolor


# constants
# classes


class ColorBox(Label):
    """Little rectangle that allows the user to select a color when clicked."""
    def __init__(self, master, default="white", **options):
        """:arg master: parent-widget
        :arg default: default color
        :arg **options: kwargs passed to Frame.__init__
        generates <<ColorChange>> event when a new color is selected.
        """
        super().__init__(master, **options)

        self._color = None
        self._set_color(default)
        self.bind("<Button-1>", self._pick_color)
        return

    def _set_color(self, color):
        """Sets color of the color box."""
        self._color = color
        self.config(bg=color)
        self.event_generate("<<ColorChange>>")
        return

    def _pick_color(self, event):
        """Generates pop-up in which user can pick a color, sets color."""
        color = askcolor(self._color)[1]  # ((r, g, b), "#0168e7")
        if color:
            self._set_color(color)
        return

    def get(self):
        """Returns selected color in hex format."""
        return self._color


class ColorMenu(LabelFrame):
    """Menu to pick specified number of colors.
    generates <<ColorChange>> event when a new color is selected.
    """
    def __init__(self, master, n, defaults=None, numbers=False, **options):
        """:arg master: parent widget
        :arg n: number of colors
        :arg defaults: default colors to use. must be length n
        :arg numbers: bool indicating whether or not to number the boxes.
        :arg options: options passed to Frame.__init__
        """
        super().__init__(master, **options)
        self._boxes = list()
        # generate widget body
        self._set_up_boxes(n, defaults, numbers)
        return

    def _set_up_boxes(self, n, defaults, add_numbers):
        """Fills the widget body."""
        # set or validate default colors
        if defaults is None:
            defaults = ["#ffffff" for _ in range(n)]
        elif len(defaults) != n:
            raise ValueError("received {} default colors. expected {}".format(len(defaults), n))

        # set up color boxes
        for n, default_color in enumerate(defaults, start=1):
            box = ColorBox(self, default=default_color, width=2, height=1, relief="ridge", bd=6)
            box.pack(side="left")
            if add_numbers:
                box.config(text=n)
            box.bind("<<ColorChange>>", lambda e: self.event_generate("<<ColorChange>>"))
            self._boxes.append(box)
        return

    def get(self):
        """Gets selected colors in hex format."""
        return [box.get() for box in self._boxes]

# functions


def hex_to_rgb(hex_value):
    """Converts hex color string '#000000' to rgb tuple, where values range from 0 to 1."""
    h = hex_value.strip("#")
    return tuple(int(h[i:i+2], 16)/255 for i in (0, 2, 4))


def main(argv=None):
    """Initiates program."""
    if argv is None:
        argv = sys.argv
    # work
    print(__doc__)
    return 0

# initiate


if __name__ == "__main__":
    sys.exit(main())
