#!/usr/bin/env python3
"""File holds the ImageDisplay class."""
# imports
import sys
import tkinter
from tkinter import ttk

from PIL import Image, ImageTk


# constants
# classes


class ImageDisplay(tkinter.Frame):
    """Widget that can display multiple images,
    putting each image in a separate named tab.
    """
    def __init__(self, master, on_tab_select=None, on_tab_close=None, **options):
        """Initializes self.
        :arg master: parent-widget
        :arg on_tab_select: callable to be run when switched to a new tab.
        callable should take the name of the tab.
        :arg on_tab_close: callback for when a tab is closed/image is removed. should take name of closed tab.
        :arg options: kwargs passed to Frame.config
        """
        super().__init__(master, **options)
        self._image_names = list()  # contains strings
        self._image_slots = list()  # contains Canvas widgets

        # build widget body
        self._tabs = ttk.Notebook(self)
        self._tabs.pack(expand=True, fill="both")

        self._tab_switch_callback = on_tab_select
        self._tab_close_callback = on_tab_close

        # event handlers
        if self._tab_switch_callback:
            # bind on_tab_select
            self._tabs.bind("<<NotebookTabChanged>>", self._on_tab_select)

        # close tab when right-clicked
        self._tabs.bind("<Button-3>", self._close_clicked_tab)
        return

    def _on_tab_select(self, event):
        """Binds self._tab_switch_callback to the selection of a new tab,
        passing it the name of the tab.
        """
        if len(self._image_slots):
            tabs = self._tabs
            self._tab_switch_callback(tabs.tab(tabs.select(), "text"))
        return

    def _close_clicked_tab(self, event):
        """Identifies which tab was clicked and closes it."""
        # if a tab label was clicked:
        if self._tabs.identify(event.x, event.y) == "label":
            tab_id = "@{event.x},{event.y}".format(event=event)
            tab_name = self._tabs.tab(tab_id, "text")
            self.remove_image(tab_name)
        return

    def _create_image_tab(self, name):
        """Creates an empty image tab with given name."""
        # create and fill tab
        tab = tkinter.Frame(self._tabs)
        y_scrollbar = tkinter.Scrollbar(tab)
        y_scrollbar.pack(fill="y", side="right")
        x_scrollbar = tkinter.Scrollbar(tab, orient="horizontal")
        x_scrollbar.pack(fill="x", side="bottom")
        canvas = tkinter.Canvas(tab,
                                yscrollcommand=y_scrollbar.set,
                                xscrollcommand=x_scrollbar.set)
        canvas.pack(expand=True, fill="both")
        y_scrollbar.config(command=canvas.yview)
        x_scrollbar.config(command=canvas.xview)

        # add tab
        self._tabs.add(tab, text=name)
        self._image_slots.append(canvas)
        return

    def display_image(self, image, image_name):
        """Display given image in a new tab named image_name.
        :arg image: an image, as accepted by PIL
        :arg image_name: "unique string", used al label on tab.
        """
        if image_name in self:
            raise KeyError("Image display already contains an image named {}.".format(image_name))
        # create new tab
        self._create_image_tab(image_name)
        self._image_names.append(image_name)
        self.update_image(image_name, image)
        self.select(image_name)
        # fix alignment of tabs
        self._repack()
        return

    def remove_image(self, image_name):
        """Removes image with given name,
        executing the on_tab_closed callback if specified for object.
        """
        if image_name not in self:
            raise KeyError("no image named {}.".format(image_name))

        index = self._image_names.index(image_name)
        self._tabs.forget(index)
        self._image_slots.pop(index)
        self._image_names.pop(index)
        self._repack()
        # tab close callback
        if self._tab_close_callback:
            self._tab_close_callback(image_name)
        return

    def update_image(self, image_name, image):
        """Replaces image with given name with given image."""
        if image_name not in self:
            raise KeyError("can not update image named {}: no such image.".format(image_name))
        index = self._image_names.index(image_name)
        # update image
        canvas = self._image_slots[index]
        canvas.delete("all")
        # convert image to tkinter-compatible image object
        canvas._image = image = ImageTk.PhotoImage(Image.fromarray(image))
        canvas.create_image(0, 0, image=image, anchor="nw")
        canvas.config(scrollregion=canvas.bbox("all"))
        return

    def select(self, name):
        """Move to/select image with given name."""
        if name not in self:
            raise ValueError("No image called " + name)
        index = self._image_names.index(name)
        self._tabs.select(index)
        return

    def _repack(self):
        """Redraws self to screen, fixing tab alignment."""
        old_pack_info = self.pack_info().copy()
        self.pack_forget()
        self.pack(**old_pack_info)
        return

    def __contains__(self, name):
        """Custom contain hook."""
        return name in self._image_names

# functions


def main(argv=None):
    """Initiates program."""
    if argv is None:
        argv = sys.argv
    # work
    print(__doc__)
    return 0

# initiate


if __name__ == "__main__":
    sys.exit(main())
