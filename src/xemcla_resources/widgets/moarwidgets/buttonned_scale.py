#!/usr/bin/env python3
"""Module for holding the tkinter class 'ButtonedScale'."""
# imports
import sys
from tkinter import Frame, Scale, Button, IntVar, DoubleVar, Label


# classes


class ButtonedScale(Frame):
    """A tkinter Scale widget that will only change its value and execute any coupled commands when the
    'set' button is clicked.
    extra options that are added:
    -buttoncolor: the color of the button that will
     "activate" the scale.
     should be a string representation of the color
    (eg. "green")

    -text: the text that should be displayed before the
     value of the scale.
    """
    def __init__(self, master, command=None, from_=0, to=100, button_color=None,
                 text=None, resolution=1, width=100, **options):
        """:param master: parent-widget
        :param command: callback that is run when value of ButtonedScale is set. Should take no arguments.
        :param from_: minimum value of scale.
        :param to: maximum value of scale.
        :param button_color: color of the 'set' button. (see Button.bg)
        :param text: text to display above the slider. e.g 'length:'
        :param resolution: step size for scale.
        :param width: widht of widget.
        :param options: options passed to tkinter.Frame
        """
        # setup self as a frame
        Frame.__init__(self, master, **options)

        # set up variables
        self._from = from_
        self._to = to
        self._command = command
        if type(resolution).__name__ == "int":
            self.value = IntVar()
        else:
            self.value = DoubleVar()

        self.value.set(0)

        # fill in self
        # dividing frames
        self.top = Frame(self)
        self.top.pack(expand=True, fill="both")

        self.bottom = Frame(self)
        self.bottom.pack(expand=True, fill="both")

        self.width_expander = Frame(self, width=width)
        self.width_expander.pack()

        # the mechanical parts
        self.display_prefix = Label(self.top, text=text)
        self.display_prefix.pack(side="left")
        self.display = Label(self.top, textvar=self.value)
        self.display.pack(side="left")

        self.scale = Scale(self.bottom, orient="horizontal",
                           from_=from_,
                           to=to,
                           resolution=resolution)
        self.scale.pack(side="right", expand=True, fill="both")

        self.button = Button(self.bottom,
                             text="set",
                             bg=button_color,
                             command=self.button_press)
        self.button.pack(pady=4)
        return

    def button_press(self):
        """Event handler for pressing the button."""
        # set self.value
        self.value.set(self.scale.get())
        
        # execute self.command if one is set
        if self._command:
            self._command()
        else:
            pass
        return

    def get(self):
        """Returns the selected value."""
        return self.value.get()

    def set(self, value):
        """Sets self.value."""
        if self._from <= value <= self._to:
            self.scale.set(value)
            self.value.set(value)
        else:
            raise ValueError("value not within allowed range: " +
                             str(value))
        return

# functions


def main(argv=None):
    """Initiates program."""
    if argv is None:
        argv = sys.argv
    # work
    print(__doc__)
    return 0

# initiate


if __name__ == "__main__":
    sys.exit(main())
