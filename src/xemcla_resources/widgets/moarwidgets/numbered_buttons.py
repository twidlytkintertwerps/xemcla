#! /usr/bin/env python3
"""Holds the NumberedButtons class."""
# imports
import sys
import tkinter


# classes


class NumberedButtons(tkinter.LabelFrame):
    """Multiple numbered buttons."""
    def __init__(self, master, n,  command=None, start=0, orient="horizontal", **options):
        """:param master: parent-widget
        :param n: number of buttons to display
        :param command: callback to run with index of pressed button
        :param start: int to start numbering from
        :param orient: orientation of buttons. either "horizontal" or "vertical"
        :param options: options passed to LabelFrame.__init__
        """
        super().__init__(master, **options)
        # set up buttons

        for index in range(start, n+start):
            b = tkinter.Button(self, text=index)
            b.pack(side="left" if orient == "horizontal" else "top")
            if command:
                b.config(command=self._link_command(command, index))
        return

    @staticmethod
    def _link_command(callback, argument):
        """links arguments to given callable.
        :param callback: callable
        :param argument: arguments to callable
        :return: callable with given arguments "wired in"
        """
        return lambda: callback(argument)

# functions


def main(argv=None):
    """Initiates program."""
    if argv is None:
        argv = sys.argv
    # work
    print(__doc__)
    return 0

# initiate


if __name__ == "__main__":
    sys.exit(main())
