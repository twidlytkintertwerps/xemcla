#!/usr/bin/env python3
"""Module that contains general classes for different kinds of scrollable listboxes."""
# imports
import sys
import tkinter as tk
import tkinter.ttk as ttk

from .scrollable import Scrollable
from ..misc import global_tkutils


# constants
# classes


class ScrollableListbox(Scrollable):
    """Class that makes a listbox scrollabe, also had some handy functions for said listbox."""
    def __init__(self, master, **listbox_pack_kwargs):
        """Initialises super class (Scrollable)
        :param master:
        See xemcla.widgets.moarwidgets.scrollable.Scrollabe documentation
        :param listbox_pack_kwargs:
        The pack arguments for the listbox. See tk.pack documentation
        """
        super().__init__(master)
        # create the scrollable listbox
        self.initialize_widgets(tk.Listbox(self, **listbox_pack_kwargs))
        self.listbox = self.otherwidget
        return

    def empty(self):
        """Empty out the listbox."""
        self.listbox.delete(0, tk.END)
        return

    def set_items(self, items):
        """Sets item given a list of classes
        :param items:
        List of items.
        """
        # empty the listbox
        self.empty()
        # add the new items (strings) to the listbox
        for item in items:
            self.listbox.insert(tk.END, item)
        return

    def get_selected_items(self):
        """Returns the names of any currently selected items."""
        listbox = self.listbox
        selected_items = [listbox.get(index) for index in listbox.curselection()]
        return selected_items


class ArrangeableScrollableListbox(ScrollableListbox):
    """Class that makes a ScrollableListbox instance arrangeable with the secondary mouse button."""
    def __init__(self, master, **listbox_pack_kwargs):
        """Initialise super.
        For params, refer to ScrollableListbox
        :param master:
        :param listbox_pack_kwargs:
        """
        super().__init__(master, **listbox_pack_kwargs)
        self.listbox.bind('<Button-3>', self.set_current)
        self.listbox.bind('<B3-Motion>', self.shift_selection)
        self.listbox.curIndex = None
        return

    def set_current(self, event):
        """Sets the current index the mouse is on currently when a bound event occurs."""
        self.listbox.curIndex = self.listbox.nearest(event.y)
        return

    def shift_selection(self, event):
        """Shifts the listbox item when dragged."""
        # get the selected items.
        selected_items = self.get_selected_items()
        i = self.listbox.nearest(event.y)
        # if the index is lower than the current index, move down.
        if i < self.listbox.curIndex:
            x = self.listbox.get(i)
            self.listbox.delete(i)
            self.listbox.insert(i + 1, x)
            # make "new" item selected if it was selected before
            if x in selected_items:
                self.listbox.selection_set(i + 1)
            self.listbox.curIndex = i
        # else move upwards.
        elif i > self.listbox.curIndex:
            x = self.listbox.get(i)
            self.listbox.delete(i)
            self.listbox.insert(i - 1, x)
            # make "new" item selected if it was selected before
            if x in selected_items:
                self.listbox.selection_set(i - 1)
            self.listbox.curIndex = i
        return


class ArrangeableValueSettableScrollableListbox(ArrangeableScrollableListbox):
    """Class that makes a ArrangleableScrollableListbox instance settable with a double click."""
    def __init__(self, master, **listbox_pack_kwargs):
        """Initialise super.
        For the follwing params, refer to ScrollableListbox;
        :param master:
        :param listbox_pack_kwargs:
        """
        super().__init__(master, **listbox_pack_kwargs)

        # set some necessary values and bind double click to correct popup function.
        self.values = {}
        self.current_item = None
        self.current_value_window = None
        self.listbox.bind('<Double-Button-1>', self.prepare_and_show_value)
        return

    def set_items(self, items):
        """Overridden method of ScrollableListbox, sets items with values.
        :param items:
        tuple of necessary values;
        (itemname, description, classtype)
        itemname is the name as it appears in the listbox, description is the description of the popup window to set
        a value, and lastly classtype is the type you want the value to be.
        :return:
        """
        # empty the listbox
        self.empty()

        # add the new items (strings) to the listbox
        for item, desc, type_ in items:
            self.listbox.insert(tk.END, item)
            self.values[item] = ("Unset", desc, type_)
        return

    def prepare_and_show_value(self, event):
        """Prepares self.values if somehow the listbox item is not defined in self.values."""
        # try to see if value is set.
        self.current_item = self.listbox.get(self.listbox.nearest(event.y))
        try:
            self.values[self.current_item]
        except:
            self.values[self.current_item] = ("Unset", "Something went wrong parsing this item", None)
        # show value-setting popup
        self.show_popup()
        return

    def show_popup(self):
        """Shows popup for setting a singular value for one listbox item. This is done using tk.Toplevel."""
        # only show popup if value is settable.
        self.current_value_window = tk.Toplevel()
        self.current_value_window.wm_title("Set value.")
        # set description
        self.current_value_window.description = tk.Label(self.current_value_window,
                                                         text=self.values[self.current_item][1])
        self.current_value_window.description.pack(side=tk.TOP)
        # make text input
        self.current_value_window.entry_field = tk.Entry(self.current_value_window)
        self.current_value_window.entry_field.insert(tk.INSERT, self.values[self.current_item][0])
        self.current_value_window.entry_field.pack()
        # make set button
        self.current_value_window.b = ttk.Button(self.current_value_window, text="Set value", command=self.set_value)
        self.current_value_window.b.pack(side=tk.BOTTOM)
        self.current_value_window.resizable(0, 0)
        # wait for visibility before setting window placement.
        self.current_value_window.wait_visibility()
        self.current_value_window = global_tkutils.window_center_placement(self.current_value_window)
        # focus window
        self.current_value_window.grab_set()
        self.current_value_window.bind("<Return>", self.set_value)
        return

    def set_value(self, *tkevent):
        """Set the value when the set value button is pressed."""

        # get the value of the entry field.
        value = self.current_value_window.entry_field.get()
        description = self.values[self.current_item][1]
        neccesary_type = self.values[self.current_item][2]

        # if type is empty, just close.
        if neccesary_type:
            # try check if the type is correct, otherwise, throw and error.
            try:
                value = neccesary_type(value)
                self.set_and_destroy(value, description, neccesary_type)
            except ValueError:
                global_tkutils.show_wrong_value_popup(neccesary_type)
        else:
            self.set_and_destroy(value, description, neccesary_type)
        return

    def set_and_destroy(self, value, description, neccesary_type):
        """Set the value in the class and destroy the toplevel window.
        The following params are the same items contained in the items tuple of function; set_items
        :param value:
        :param description:
        :param neccesary_type:
        :return:
        """
        print("set listbox item ({}) value; ({})".format(self.current_item, value))
        # set value in memory
        self.values[self.current_item] = (value, description, neccesary_type)
        # destroy the window
        self.current_value_window.destroy()
        return

    def retrieve_values(self, only_selected=True):
        """Returns values in a dictionary of each item in the listbox
        :param only_selected:
        If set to true, only the selected items get returned. (this is also the standard behaviour)

        :return:
        dictionary;
        key: The item name
        value: The set value
        """
        total = {}
        for key, value in self.values.items():
            if only_selected:
                if key in self.get_selected_items():
                    total[key] = value
            else:
                total[key] = value
        return total

# functions


def main(argv=None):
    """Initiates program."""
    if argv is None:
        argv = sys.argv
    # work
    print(__doc__)
    return 0

# initiate


if __name__ == "__main__":
    sys.exit(main())
