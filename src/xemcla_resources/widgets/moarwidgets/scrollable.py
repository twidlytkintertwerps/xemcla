#!/usr/bin/env python3
"""Module that contains general class that makes any tkinter wigdet scrollable."""
# imports
import sys
import tkinter as tk


# constants
# classes


class Scrollable(tk.Frame):
    """General class that makes any tkinter wigdet scrollable."""
    def __init__(self, master):
        """Initiates the super (tk.Frame) when making object.
        :param master:
        see the tk.Frame documentation.
        """
        # run init of frame
        super().__init__(master)
        self.x_scrollbar = None
        self.y_scrollbar = None
        self.otherwidget = None
        return

    def initialize_widgets(self, otherwidget, **pack_kwargs):
        """Creates the correct view given another widget that needs to be made scrollable.
        :param otherwidget:
        The other widget, this can be any tkinter widget.
        :param pack_kwargs:
        The pack arguments for the 'other widget'. See tk.pack documentation
        """
        # create the scrollbars
        self.x_scrollbar = tk.Scrollbar(self, orient=tk.HORIZONTAL)
        self.x_scrollbar.pack(side=tk.BOTTOM, fill=tk.X)

        self.y_scrollbar = tk.Scrollbar(self, orient=tk.VERTICAL)
        self.y_scrollbar.pack(side=tk.RIGHT, fill=tk.Y)

        # create the other widget
        self.otherwidget = otherwidget
        self.otherwidget.config(xscrollcommand=self.x_scrollbar.set, yscrollcommand=self.y_scrollbar.set)
        self.otherwidget.pack(**pack_kwargs)
        # bind the scrollbars to the text frame
        self.x_scrollbar.config(command=self.otherwidget.xview)
        self.y_scrollbar.config(command=self.otherwidget.yview)
        return

# functions


def main(argv=None):
    """Initiates program."""
    if argv is None:
        argv = sys.argv
    # work
    print(__doc__)
    return 0

# initiate


if __name__ == "__main__":
    sys.exit(main())
