#!/usr/bin/env python3
"""Holds the CentroidLegend class."""
# imports
import sys
import tkinter
from tkinter import ttk


# constants
# classes


class CentroidLegend(tkinter.Frame):
    """Widget wor displaying the locations of centroids in data-space."""
    def __init__(self, master, centroids, axis_labels, **options):
        """:param master: parent-widget
        :param centroids: locations of cluster centoids in data-space: collection of tuples or similar
        :param axis_labels: names of dimensions in data space
        :param options: options passed to Frame.__init__
        """
        super().__init__(master, **options)
        self._centroids = centroids
        self._labels = axis_labels

        # setup table
        self._tree = ttk.Treeview(self, columns=axis_labels)
        self._tree.heading("#0", text="cluster")

        for n, label in enumerate(axis_labels):
            self._tree.heading(n, text=label)
            self._tree.column(n, anchor="center", width=60, stretch=0)
        self._tree.column("#0", anchor="center", width=70)
        for n, coordinate in enumerate(centroids, start=1):
            coordinate = list(map(lambda x: round(x, 2), coordinate))
            self._tree.insert("", "end", text=str(n), values=coordinate)
        self._tree.pack(expand=True, fill="both")
        return

# functions


def main(argv=None):
    """Initiates program."""
    if argv is None:
        argv = sys.argv
    # work
    print(__doc__)
    return 0

# initiate


if __name__ == "__main__":
    sys.exit(main())
