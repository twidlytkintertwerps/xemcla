#! /usr/bin/env python3
"""Holds class for representing tkinter tab widget responsible for preprocessing of edx data."""
# imports
import os
import sys
import time
import tkinter as tk
import warnings
from multiprocessing import Process

import imageio
import numpy as np
import skimage

from xemcla_resources.image_processing import filtering
from xemcla_resources.io.csv_utils import read_csv
from xemcla_resources.io.save_image import save_image
from .notebook_tab import NotebookTab
from ..misc import global_tkutils
from ..moarwidgets.scrollable_listboxes import ArrangeableValueSettableScrollableListbox


# constants
# classes


class PreprocessingTab(NotebookTab):
    """Preprocessing tab for representing the preprocessing options and making it runnable in the gui."""
    def __init__(self, master, root):
        """Initialize all widgets and necessary storage values
        :param master:
        Master window, see tk documentation
        :param root:
        Root window, see tk documentation
        """
        # ### set tab options here ###
        # see https://docs.python.org/3.1/library/tkinter.ttk.html#tab-options
        tab_options = dict(text="Preprocessing")
        super().__init__(master, root, **tab_options)

        # storage values.
        self.global_amount_ran = 0
        self.an_error_occured = True
        self.csv_files = []
        self.element_block = None
        self.main_image = None
        self.main_image_filtered = None
        self.used_filter_params = None
        self.output_directory = None
        self.old_shape = None

        # ### construct contents of tab here ###

        # make new listbox for the filtering methods.
        self.filters = ArrangeableValueSettableScrollableListbox(self,
                                                                 selectmode=tk.MULTIPLE,
                                                                 selectborderwidth=0,
                                                                 exportselection=0,
                                                                 activestyle="none",
                                                                 selectbackground="#90EE90")
        self.gaus = "gaussian"
        self.tv = "total variance"
        self.bilateral = "bilateral"
        self.wavelet = "wavelet"
        self.nl_means = "non-local means"

        self.filters_name_descriptor = {self.gaus: ("gaus", "sigma"),
                                        self.tv: ("tv", "weight"),
                                        self.bilateral: (self.bilateral, "sigma_spatial"),
                                        self.wavelet: (self.wavelet, "sigma"),
                                        self.nl_means: ("non-local_means", "h")
                                        }
        self.filters.set_items([(self.gaus, "please specify gaus sigma:", int),
                                (self.tv, "please specify tv weight:", float),
                                (self.bilateral, "please specify the spatial sigma for the bilateral function", float),
                                (self.wavelet, "please specify the wavelet sigma", float),
                                (self.nl_means, "please specify the Cut-off distance of non-local means", float)
                                ])
        self.filters.listbox.selection_set(0, 1)
        self.filters.values[self.gaus] = (1, "please specify gaus sigma:", int)
        self.filters.values[self.tv] = (.8, "please specify tv weight:", float)

        # part for selecting files
        # filebox for filenames
        self.select_files_box = tk.Text(self)
        self.select_files_box.config(state=tk.DISABLED)
        # frame with button
        self.select_files_frame = tk.Frame(self)
        self.select_files_text = tk.Label(self.select_files_frame, text="Select files:")
        self.select_files_button = tk.Button(self.select_files_frame, text="Browse", command=lambda: global_tkutils.
                                             browse_files_function(self.select_files_box))

        # part for selecting the output directory.
        self.select_dir_box = tk.Text(self)
        self.select_dir_box.config(state=tk.DISABLED)
        # frame with button
        self.select_dir_frame = tk.Frame(self)
        self.select_dir_text = tk.Label(self.select_dir_frame, text="Select output directory:")
        self.select_dir_button = tk.Button(self.select_dir_frame, text="Browse", command=lambda: global_tkutils.
                                           browse_dir_function(self.select_dir_box))

        # run button
        # SCALE YES/NO
        self.run_button_frame = tk.Frame(self)
        self.scalevar = tk.BooleanVar()
        self.show_var = tk.BooleanVar()
        self.scalevar.set(True)
        self.show_var.set(False)
        self.scale = tk.Checkbutton(self.run_button_frame, text="Withen Scale?", variable=self.scalevar)
        self.show = tk.Checkbutton(self.run_button_frame, text="Show in view?", variable=self.show_var)
        self.run_button = tk.Button(self.run_button_frame, text="Run this step", command=self._run)

        # pack n' grid
        self.select_files_text.pack()
        self.select_files_button.pack(side=tk.LEFT)
        self.select_dir_text.pack()
        self.select_dir_button.pack(side=tk.LEFT)
        self.filters.grid(row=0, column=0)
        self.select_files_frame.grid(row=0, column=1, sticky=tk.NW)
        self.select_files_box.grid(row=0, column=2, sticky=tk.NW)
        self.select_dir_frame.grid(row=1, column=1, sticky=tk.NW)
        self.select_dir_box.grid(row=1, column=2, sticky=tk.NW)
        self.scale.pack()
        self.show.pack()
        self.run_button.pack()
        self.run_button_frame.grid(row=1, column=3)

        # list with all buttons
        self._buttons = [self.select_files_button, self.select_dir_button, self.scale, self.show, self.run_button]
        return

    def _lock(self):
        """disables all buttons on the tab"""
        print("Process start requested, Locking Buttons")
        for button in self._buttons:
            button.config(state="disabled")
        return

    def _unlock(self):
        """enables all locked buttons"""
        print("Process finished, Unlocking Buttons")
        for button in self._buttons:
            button.config(state="normal")
        return

    def _run(self):
        """runs preprocessing, managing locking of buttons"""
        self._lock()
        self.run_all_filters()
        self._unlock()
        return

    def rebuild_filter_params(self, selected, my_values):
        """From the gui's settings, rebuild the filtering parameters into it's proper data value.
        :param selected:
        The selected items (list)
        :param my_values:
        The values they hold (dict)
        :return:
        Return the correct params, see xemcla_resources.image_processing.filtering.prepare_image documentation.
        """
        # store new params in the list.
        new_params = []
        for gui_name, filter_opt_tuple in self.filters_name_descriptor.items():
            # if the value is in selected, add it to the params.
            if gui_name in selected:
                val = my_values[gui_name][0]
                if val != "Unset":
                    # first item in tuple is filter name
                    # second item is the variable name
                    new_params.append((filter_opt_tuple[0], {filter_opt_tuple[1]: float(val)}))
        # return new params
        return new_params

    def run_all_filters(self):
        """Runs all filters when run button is pressed.
        Following params are specified in gui;
        :param value_settable_listbox_instance:
        :param filters_value_descriptor:
        :param image_files:
        :param output_directory:
        :return:
        """
        # set some variables.
        # first update status bar
        self.root.status_bar.display("Preprocessing images...")
        self.update()
        warnings.filterwarnings("ignore")
        start_time = time.time()
        final_time = None
        self.an_error_occured = False
        self.global_amount_ran += 1
        amount_normal_images = 0
        self.csv_files = []
        # unpack some variables
        image_files = global_tkutils.retrieve_files_from_textbox(self.select_files_box)
        self.output_directory = global_tkutils.retrieve_dir_from_textbox(self.select_dir_box)
        selected = self.filters.get_selected_items()
        my_values = self.filters.values
        # throw errors if something (any input) is missing.
        if not selected:
            self.an_error_occured = True
            error_string = "No filters were selected! (preprocessing tab)"
            print(error_string)
            global_tkutils.general_popup("Error!", error_string, "Ok.")
        if not self.output_directory:
            self.an_error_occured = True
            error_string = "No output directory set! (preprocessing tab)"
            print(error_string)
            global_tkutils.general_popup("Error!", error_string, "Ok.")
        else:
            # reconstruct parameters for filter function
            new_params = self.rebuild_filter_params(selected, my_values)

            if not new_params:
                self.an_error_occured = True
                error_string = "Incorrect filtering settings! (preprocessing tab)"
                print(error_string)
                global_tkutils.general_popup("Error!", error_string, "Ok.")
            else:
                self.used_filter_params = new_params
                # recursively get images and types
                opened_image = None
                warn_1 = False
                error_string_warning_1 = []
                for imfile in image_files:
                    # try to open the image if possible.
                    try:
                        # if this try works, this means an image file has been selected.
                        opened_image = skimage.color.rgb2grey(imageio.imread(imfile))  # inline conversion to grayscale.
                        if len(opened_image) == 3:  # if still multi-channel, convert to single.
                            opened_image = opened_image[:, :, 0]
                        print("Normal image selected; \"" + imfile + "\"")
                        # opened_image = img_as_ubyte(opened_image)
                        self.main_image = imfile
                        amount_normal_images += 1
                    except ValueError:
                        try:
                            # csv image if this try works.
                            read_csv(imfile, sep=",")
                            print("csv element selected; \"" + imfile + "\"")
                            self.csv_files.append(imfile)
                        except Exception as err:
                            self.an_error_occured = True
                            error_string_warning_1.append("Something went wrong while trying to read your specified "
                                                          "file, is it a correct format?. \"{}\" "
                                                          "(preprocessing tab)".format(imfile))
                            print(error_string_warning_1[-1])
                            print(err)
                            warn_1 = True
                            pass
                # before doing anything else, first check if there is only 1 normal image files and 1 or more csv's
                # and if dimensionality is the same for all files.
                warn_2 = False
                error_string_warning_2 = []
                if amount_normal_images > 1:
                    error_string_warning_2.append("Warning!; Too many images were selected, only 1 is needed, using "
                                                  "last selected image; {} (preprocessing tab)".format(self.main_image))
                    print(error_string_warning_2[-1])
                    warn_2 = True

                if not self.csv_files:
                    self.an_error_occured = True
                    error_string = "CRITICAL; NO CSV'S SELECTED OR DETECTED! (preprocessing tab)"
                    print(error_string)
                    global_tkutils.general_popup("Error!", error_string, "Ok.")
                elif not amount_normal_images > 0:
                    self.an_error_occured = True
                    error_string = "CRITICAL; NO IMAGES SELECTED OR DETECTED! (preprocessing tab)"
                    print(error_string)
                    global_tkutils.general_popup("Error!", error_string, "Ok.")
                # check dimensionality
                elif not all(opened_image.shape == read_csv(csv, sep=",").shape for csv in self.csv_files):
                    self.an_error_occured = True
                    error_string = "CRITICAL; FILES DO NOT HAVE SAME DIMENSIONALITY! (preprocessing tab)"
                    print(error_string)
                    global_tkutils.general_popup("Error!", error_string, "Ok.")
                else:
                    try:
                        print("---\nStarted filtering...")
                        # first prepare and filter the em image
                        # after reading in the image, do the filtering step for the main image.
                        filtered_image = filtering.prepare_image(opened_image, new_params)
                        # save the file.
                        image_name = ".".join(os.path.basename(self.main_image).split(".")[:-1]) + \
                                     "_preprocessed_run{}".format(self.global_amount_ran)
                        outfile = self.output_directory + "/" + image_name + ".png"
                        self.main_image_filtered = outfile
                        print("Saving filtered em image. ({})".format(outfile))
                        save_image(outfile, filtered_image)
                        # send main image to view
                        if self.show_var.get():
                            self.root.view_tab.register_image(imageio.imread(outfile), os.path.basename(outfile))

                        # now filter all elements
                        self.element_block, elements, self.old_shape = filtering.preprocess_edx_data(
                            self.csv_files,
                            new_params,
                            self.scalevar.get()
                        )
                        # now unpack this and send all elements to the view.
                        csv_elements_save_processes = list()
                        csv_element_outfiles = list()
                        for packed_filtered_element in zip(self.csv_files, self.element_block.T):
                            csv_file = packed_filtered_element[0]
                            basename = os.path.basename(csv_file).split(".")[0]
                            data = packed_filtered_element[1]
                            # reconstruct the image
                            reconstructed_image = np.array(data).reshape(self.old_shape)
                            # save the image in the output dir.
                            outfile = self.output_directory + "/" + basename + "_preprocessed_run{}.png".format(
                                self.global_amount_ran)
                            print("Saving filtered csv file to png ({})".format(outfile))
                            csv_element_outfiles.append(outfile)
                            # each element is saved in parallel
                            p = Process(target=save_image, args=(outfile, reconstructed_image))
                            p.start()
                            csv_elements_save_processes.append(p)
                        # wait for images to be saved.
                        for process in csv_elements_save_processes:
                            process.join()
                        # send the images to the view
                        for csv_element_outfile in csv_element_outfiles:
                            if self.show_var.get():
                                self.root.view_tab.register_image(imageio.imread(csv_element_outfile),
                                                                  os.path.basename(csv_element_outfile))
                        # last but not least, save used settings to a file;
                        prepparamsfile = self.output_directory+"/prep_opts_run{}.txt".format(self.global_amount_ran)
                        print("Saving used filtering options to: ({})".format(prepparamsfile))
                        prepparams = open(prepparamsfile, "w+")
                        content = "run number;\n{}\nused image;\n{}\nused csv files;\n{}\nfilter options;\n{}\nwithen" \
                                  " scale yes/no;\n{}\n".format(self.global_amount_ran, self.main_image, self.csv_files,
                                                                new_params, self.scalevar.get())
                        prepparams.write(content)
                        prepparams.close()
                        print("Filtering done!")
                        final_time = time.time()-start_time
                        print("Filtering took {:.2f}s".format(final_time))
                        if warn_1:
                            for error in error_string_warning_1:
                                global_tkutils.general_popup("Warning!", error, "Ok.")
                        if warn_2:
                            for error in error_string_warning_2:
                                global_tkutils.general_popup("Warning!", error, "Ok.")
                        if self.show_var.get():
                            # navigate to "view" tab
                            self.root.tabs.select(self.root.view_tab)
                    except:
                        self.an_error_occured = True
                        error_string = "CRITICAL; FILTERING EXITED WITH AN ERROR! - " \
                                       "(are you using non-local means in some combinations this might fail) " \
                                       "(preprocessing tab)"
                        print(error_string)
                        global_tkutils.general_popup("Error!", error_string, "Ok.")
        # update status bar
        if final_time:
            self.root.status_bar.display("Done filtering! took; {:.2f}s".format(final_time))
        else:
            self.root.status_bar.clear()
        self.update()
        # update gui so warnings, errors and popups don't show prematurely or unpack
        self.root.update()
        return

# functions


def main(argv=None):
    """Initiates program."""
    if argv is None:
        argv = sys.argv
    # work
    print(__doc__)
    return 0

# initiate


if __name__ == "__main__":
    sys.exit(main())
