#! /usr/bin/env python3
"""This module holds classes representing a tkinter widget tab responsible for clustering of pixels."""
# imports
import os
import sys
import time
import tkinter as tk

import imageio
from scipy.cluster.vq import kmeans2 as scvq_kmeans
from skimage import img_as_ubyte

from xemcla_resources.io.kmeans_result_object import KmeansResultObject
from xemcla_resources.statistics import clustering
from .notebook_tab import NotebookTab
from ..misc import global_tkutils
from ..moarwidgets.scrollable_listboxes import ScrollableListbox


# constants
# classes


class ClusteringTab(NotebookTab):
    """ClusteringTab for displaying clustering options. and running it in the gui."""
    def __init__(self, master, root):
        """Runs super class init and specifies+packs all necessary widgets.
        :param master:
        Master to be packing widgets inside of
        :param root:
        Root window of the program. Needed for relating to other tabs.
        """
        # set tab options here
        # see https://docs.python.org/3.1/library/tkinter.ttk.html#tab-options
        tab_options = dict(text="Clustering")
        super().__init__(master, root, **tab_options)

        # keep track of some global variables that might be handy in a new tab.
        self.output = None
        self.an_error_occured = True
        self.amount_ran = 0

        # ### construct contents of tab here ###

        # vars;

        # verbose - bool
        # nstart - int
        # cluster threshold - float (between 0-1)
        # k - int
        # projection - listbox selection
        # use previous preprocessing - bool

        # make new text input field
        self.verbosevar = tk.BooleanVar()
        self.verbosevar.set(True)
        self.verbose = tk.Checkbutton(self, text="Verbosity", variable=self.verbosevar)

        # make an input for the nstart argument
        self.nstart_frame = tk.Frame(self)
        self.nstart_label = tk.Label(self.nstart_frame, text="nstart (number of iterations to peform kmeans "
                                                             "clustering)")
        self.nstart = tk.Entry(self.nstart_frame)  # .get()
        self.nstart.insert(0, 20)

        # make an input for the clusterthing threshold
        self.auto_k_threshold_frame = tk.Frame(self)
        self.auto_k_threshold_label = tk.Label(self.auto_k_threshold_frame, text="clustering threshold decimal "
                                                                                 "percentile, has to be between "
                                                                                 "0 and 1. (can be a float) - "
                                                                                 "(used with autokmeans)")
        self.auto_k_threshold = tk.Entry(self.auto_k_threshold_frame)  # .get()
        self.auto_k_threshold.insert(0, 0.45)

        # make an input for single k option.
        self.k_frame = tk.Frame(self)
        self.k = tk.Entry(self.k_frame)  # .get()
        self.k.insert(0, 5)
        self.k.config(state=tk.DISABLED)
        self.k_single_variable = tk.BooleanVar()
        self.k_single_checkbox = tk.Checkbutton(self.k_frame,
                                                text="Specify a set K. (Must be between 0 and infinity) - "
                                                     "(used with single kmeans)",
                                                variable=self.k_single_variable,
                                                command=self.toggle_kmeans_option_in_gui)

        # projection setting for setting either the original image or filtered image to be used in the view.
        self.projection_setting = ScrollableListbox(self, selectmode=tk.BROWSE,
                                                    selectborderwidth=0,
                                                    exportselection=0,
                                                    activestyle="none",
                                                    selectbackground="#90EE90")
        self.projection_setting.set_items(["Original EM", "Filtered EM"])
        self.projection_setting.listbox.selection_set(0)

        # define run button with "use previous preprocessing" option.
        self.run_button_frame = tk.Frame(self)
        self.use_previous_preprocessing_run = tk.BooleanVar()
        self.use_previous_preprocessing_run.set(True)
        self.run_button_frame.use_previous_preprocessing_run = tk.Checkbutton(self.run_button_frame,
                                                                              text="Use previous preprocessing run.",
                                                                              variable=
                                                                              self.use_previous_preprocessing_run)
        self.run_button = tk.Button(self.run_button_frame, text="Run!", command=self._run)

        # pack everything
        self.verbose.grid(row=0, column=0, sticky=tk.W)

        self.nstart_label.grid(row=0, column=1)
        self.nstart.grid(row=0, column=0)
        self.nstart_frame.grid(row=1, column=0, sticky=tk.W)

        self.auto_k_threshold_label.grid(row=0, column=1)
        self.auto_k_threshold.grid(row=0, column=0)
        self.auto_k_threshold_frame.grid(row=2, column=0, sticky=tk.W)

        self.k_single_checkbox.grid(row=0, column=1)
        self.k.grid(row=0, column=0)
        self.k_frame.grid(row=3, column=0, sticky=tk.W)

        self.projection_setting.grid(row=4, column=0, sticky=tk.W)

        self.run_button_frame.use_previous_preprocessing_run.grid(row=0, column=0, sticky=tk.W)
        self.run_button.grid(row=1, column=0, sticky=tk.W)
        self.run_button_frame.grid(row=5, column=0, sticky=tk.W)

        # list of widgets to disable on self._lock
        self._locking_widgets = [self.verbose, self.nstart, self.auto_k_threshold, self.k,
                                 self.k_single_checkbox, self.run_button_frame.use_previous_preprocessing_run,
                                 self.run_button]
        return

    def _lock(self):
        """Disables all buttons on the tab."""
        for button in self._locking_widgets:
            button.config(state="disabled")
        return

    def _unlock(self):
        """Enables all locked buttons."""
        for button in self._locking_widgets:
            button.config(state="normal")
        self.toggle_kmeans_option_in_gui()
        return

    def _run(self):
        """Executes clustering, managing locking of button."""
        self._lock()
        self.run_clustering()
        self._unlock()
        return

    def run_singlekmeans(self, k, nstart, prepped_images, image_size, element_labels, projection_image):
        """Runs single kmeans if this has been specified.
        :param k:
        see scipy.cluster.vq.kmeans2 documentation.
        :param nstart:
        see scipy.cluster.vq.kmeans2 documentation.
        :param prepped_images:
        see xemcla_resources.image_processing.filtering.preprocess_edx_data stacked_images return variable.
        :param image_size:
        see xemcla_resources.image_processing.filtering.preprocess_edx_data prepped_images[0].shape return variable.
        :param element_labels:
        see xemcla_resources.image_processing.filtering.preprocess_edx_data elements return variable
        :param projection_image:
        Image to be used to display clusters on top of.
        :return:
        KmeansResultObject instance, used for pickling (saving) the cluster results.
        """
        # first run the kmeans
        centroids, labels = scvq_kmeans(prepped_images, k, missing="raise", iter=nstart)
        # Calculate score too.
        score = clustering.calc_score_ratio(prepped_images, centroids, labels)

        return self.register_to_view_and_give_results(projection_image, labels, centroids, element_labels, score,
                                                      image_size)

    def run_autokmeans(self, auto_k_threshold, nstart, verbose, prepped_images, image_size, element_labels,
                       projection_image):
        """Runs auto kmeans if this has been specified.
        :param auto_k_threshold:
        see xemcla_resources.statistics.clustering.auto_kmeans
        :param nstart:
        see scipy.cluster.vq.kmeans2 documentation.
        :param verbose:
        see xemcla_resources.statistics.clustering.auto_kmeans
        :param prepped_images:
        see xemcla_resources.image_processing.filtering.preprocess_edx_data stacked_images return variable.
        :param image_size:
        see xemcla_resources.image_processing.filtering.preprocess_edx_data prepped_images[0].shape return variable.
        :param element_labels:
        see xemcla_resources.image_processing.filtering.preprocess_edx_data elements return variable
        :param projection_image:
        Image to be used to display clusters on top of.
        :return:
        KmeansResultObject instance, used for pickling (saving) the cluster results.
        """
        labels, centroids, score = clustering.auto_kmeans(prepped_images, auto_k_threshold,
                                                          iter=nstart, verbose=verbose)
        return self.register_to_view_and_give_results(projection_image, labels, centroids, element_labels, score,
                                                      image_size)

    def register_to_view_and_give_results(self, projection_image, labels, centroids, element_labels, score, image_size):
        """Registers clustering to the view tab and returns it in a KmeansResultObject
        :param projection_image:
        Image to be used to display clusters on top of.
        :param labels:
        see scipy.cluster.vq.kmeans2 documentation.
        :param centroids:
        see scipy.cluster.vq.kmeans2 documentation.
        :param element_labels:
        see xemcla_resources.image_processing.filtering.preprocess_edx_data elements return variable
        :param score:
        see xemcla_resources.statistics.clustering.calc_score_ratio
        :param image_size:
        see xemcla_resources.image_processing.filtering.preprocess_edx_data prepped_images[0].shape return variable.

        :return:
        KmeansResultObject instance, used for pickling (saving) the cluster results.
        """
        # properly reshape labels to correct size.
        image_labels = labels.reshape(*image_size)

        # read in em_image and reshape if neccesary
        em_image = img_as_ubyte(imageio.imread(projection_image))
        if len(em_image.shape) == 3:
            em_image = em_image[:, :, 0]
        # set output.
        self.output = KmeansResultObject(em_image,
                                         image_labels,
                                         centroids.tolist(),
                                         element_labels,
                                         score,
                                         "kmeans_clust_run{}".format(self.amount_ran))
        # register image
        self.root.view_tab.register_cluster_image(*self.output.get_view_args())
        # return results
        return self.output

    def toggle_kmeans_option_in_gui(self, *event):
        """Toggles option in gui to either use the autokmeans or single kmeans."""
        # toggle boolean first.
        if self.k_single_variable.get():
            print("turning k opt on")
            self.k.config(state=tk.NORMAL)
            self.auto_k_threshold.config(state=tk.DISABLED)
        else:
            print("turning k opt off")
            self.k.config(state=tk.DISABLED)
            self.auto_k_threshold.config(state=tk.NORMAL)
        return

    def run_clustering(self):
        """Run all checks and keep track of variables and then do a clustering of the data."""
        # set/increment some vars
        start_time = time.time()
        final_time = None
        self.amount_ran += 1
        self.an_error_occured = False
        # get values from gui and check the inputs if they are correct.
        verbose = self.verbosevar
        # nstart
        try:
            nstart = int(self.nstart.get())
        except ValueError:
            self.an_error_occured = True
            error_string = "nstart should be an integer! (clustering tab)"
            print(error_string)
            global_tkutils.general_popup("Error!", error_string, "Ok.")
        # auto_k_threshold
        try:
            auto_k_threshold = float(self.auto_k_threshold.get())
            if auto_k_threshold > 1 or auto_k_threshold < 0:
                raise ValueError
        except ValueError:
            self.an_error_occured = True
            error_string = "clustering threshold should be a float and in between 0-1! (clustering tab)"
            print(error_string)
            global_tkutils.general_popup("Error!", error_string, "Ok.")

        # get k
        try:
            k = int(self.k.get())
            if k < 1:
                raise ValueError
        except ValueError:
            self.an_error_occured = True
            error_string = "k should be an integer and higher than 0! (clustering tab)"
            print(error_string)
            global_tkutils.general_popup("Error!", error_string, "Ok.")

        if not self.an_error_occured:
            use_previous_preprocessing = self.use_previous_preprocessing_run.get()
            if not use_previous_preprocessing or self.root.prep_tab.an_error_occured:
                print("No previous run found or last preprocessing has crashed, trying to run preprocessing anyway...")
                self.root.prep_tab.run_all_filters()
            # if no error(s) has occured, continue.
            if not self.root.prep_tab.an_error_occured:
                print("---\nStarted clustering.")
                # change status bar
                self.root.status_bar.display("Clustering pixels...")
                self.update()
                # check if kmeans or autokmeans need to be run
                prepped_images = self.root.prep_tab.element_block
                image_size = self.root.prep_tab.old_shape
                element_labels = [os.path.basename(element_file) for element_file in self.root.prep_tab.csv_files]
                if self.projection_setting.get_selected_items()[0] == "Original EM":
                    projection_image = self.root.prep_tab.main_image
                else:
                    projection_image = self.root.prep_tab.main_image_filtered

                if self.k_single_variable.get():
                    kmeans_obj = self.run_singlekmeans(k, nstart, prepped_images, image_size, element_labels,
                                                       projection_image)
                else:
                    kmeans_obj = self.run_autokmeans(auto_k_threshold, nstart, verbose, prepped_images, image_size,
                                                     element_labels, projection_image)
                # last but not least, save used settings to a file;

                clustfile = self.root.prep_tab.output_directory + "/clust_opts_run{}.txt".format(self.amount_ran)
                print("Saving used clustering settings to ({})".format(clustfile))
                clustopts = open(clustfile, "w+")
                content = "run number;\n{}\nnstart;\n{}\nautokmeans (false) or specified k (true);\n{}\nthreshold " \
                          "(only applicable in autokmeans);\n{}\nk (only applicable with specified k);\n{}\n" \
                          "use previous run?;\n{}\n".format(self.amount_ran, nstart,
                                                            self.k_single_variable.get(),
                                                            auto_k_threshold, k, use_previous_preprocessing)
                clustopts.write(content)
                clustopts.close()
                # also save the kmeans result..
                pickle_outfile = self.root.prep_tab.output_directory + \
                    "/clust_result_run{}.pickle".format(self.amount_ran)
                print("Saving clustering into pickle file; ({})".format(pickle_outfile))
                kmeans_obj.put_cucumber_in_brine(pickle_outfile)
                centroid_table_outfile = self.root.prep_tab.output_directory + \
                    "/clust_result_centroid_table_run{}.csv".format(self.amount_ran)
                print("Saving centroid table; ({})".format(centroid_table_outfile))
                kmeans_obj.save_centroid_table_to_csv(centroid_table_outfile)
                print("Clustering done!")
                final_time = time.time() - start_time
                print("Clustering took: {:.2f}s".format(final_time))
                # send to view tab
                self.root.tabs.select(self.root.view_tab)
        # update status bar
        if final_time:
            self.root.status_bar.display("Done clustering! took; {:.2f}s".format(final_time))
        else:
            self.root.status_bar.clear()
        self.update()
        # update gui so warnings, errors and popups don't show prematurely or unpack
        self.root.update()
        return

# functions


def main(argv=None):
    """Initiates program."""
    if argv is None:
        argv = sys.argv
    # work
    print(__doc__)
    return 0

# initiate


if __name__ == "__main__":
    sys.exit(main())