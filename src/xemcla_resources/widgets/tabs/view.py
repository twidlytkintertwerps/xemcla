#! /usr/bin/env python3
"""Holds the "view" tab"""
# imports
import pickle
import sys
import tkinter
import tkinter.filedialog as tkf

from .notebook_tab import NotebookTab
from ..misc import global_tkutils
from ..moarwidgets.image_display import ImageDisplay
from ..moarwidgets.view_menu import ImageMenu, ClusterImageMenu


# constants
# classes


class ViewTab(NotebookTab):
    """The "view" tab."""
    def __init__(self, master, root, **tab_options):
        """Initialize self.
        :arg master: ttk.Notebook
        :arg root: root of application: Tk instance
        :arg tab_options: keyword arguments for tab
        (see https://docs.python.org/3.1/library/tkinter.ttk.html#tab-options)
        """
        tab_options["text"] = "View"
        super().__init__(master, root, **tab_options)
        self._menus = dict()
        self.amount_double = 0

        # keep track of the amount of pickles loaded.
        self.pickle_loads = 0

        # ### construct contents of tab here ###
        # menu area
        self._menu_area = tkinter.Frame(self, bg="#82a3ff")
        self._menu_area.pack(expand=False, fill="y", side="right")
        # button for loading pickled results
        self._pickle_button = tkinter.Button(self._menu_area, text="Load previous clustering analysis "
                                                                   "(.pickle file).", command=self.extract_from_brine)
        self._pickle_button.grid(row=0, column=0)

        # display area
        self.display = ImageDisplay(self,
                                    on_tab_select=self._raise_menu,
                                    on_tab_close=self._unregister_image)
        self.display.pack(expand=True, fill="both", side="left")
        return

    def _raise_menu(self, name):
        """Raises specified image menu to the top.
        :arg name: name of image for which to bring up corresponding menu.
        """
        self._menus[name].tkraise()
        return

    def _unregister_image(self, name):
        """'Forgets' image with given name."""
        self._menus[name].destroy()
        self._menus.pop(name)
        return

    def register_cluster_image(self, em_image, label_image, centers, coordinate_labels, score, name):
        """Adds a k-means image to the view tab, generating the associated menu items.
        :arg em_image: grayscale em image: 2D np array(unit-8)
        :arg label_image: 2D np array(same size as em_image) containing the cluster label for every pixel(int)
        :arg centers: centroids of the clustering / cluster means: collection of coordinates in nD space
        :arg coordinate_labels: axis labels/names for the coordinates described under 'centers'
        :arg score: between_ss / total_ss for the clustering
        :arg name: name/label(str) for image tab. must be unique.
        """
        # logic
        # name = "labels_" + name
        # make new menu item
        image_menu = ClusterImageMenu(self._menu_area, self.root, self, self.display, em_image, label_image,
                                      centers, coordinate_labels, score, name)
        if name in self.display:
            self.amount_double += 1
            name += "_dup" + str(self.amount_double)
            image_menu = ClusterImageMenu(self._menu_area, self.root, self, self.display, em_image, label_image,
                                          centers, coordinate_labels, score, name)
        image_menu.grid(row=1, column=0, sticky="nsew")
        # save references
        self._menus[name] = image_menu
        # show images
        self.display.display_image(em_image, name)
        return

    def register_image(self, image, name):
        """Adds a regular image to the view tab. Simple display.
        :arg image: image (np array) as recognized by PIL.
        :arg name: name to use as a label for the image.
        """
        # make new image menu
        image_menu = ImageMenu(self._menu_area, self.root, self, self.display, image, name)
        if name in self.display:
            self.amount_double += 1
            name += "_dup" + str(self.amount_double)
            image_menu = ImageMenu(self._menu_area, self.root, self, self.display, image, name)
        image_menu.grid(row=1, column=0, sticky="nsew")
        # save reference
        self._menus[name] = image_menu
        # display image
        self.display.display_image(image, name)
        return

    def extract_from_brine(self):
        """Load cluster results from pickle file."""
        filename = tkf.askopenfile(filetypes=(("pickle kmeans file", "*.pickle"),("all files","*.*")))
        if filename:
            try:
                filename = filename.name
                with open(filename, "rb") as opened:
                    image, image_labels, centroids, element_labels, score, tabname = pickle.load(opened).get_view_args()
                    tabname += "_pickle"+str(self.pickle_loads+1)
                    self.pickle_loads += 1
                    self.register_cluster_image(image, image_labels, centroids, element_labels, score, tabname)
            except Exception as err:
                error_message = "Could not load pickle file because of an error!"
                print(err)
                print(error_message)
                global_tkutils.general_popup("Error!", error_message, "Ok.")
        return


# functions
def main(argv=None):
    """Initiates program."""
    if argv is None:
        argv = sys.argv
    # work
    return 0


# initiate
if __name__ == "__main__":
    sys.exit(main())
