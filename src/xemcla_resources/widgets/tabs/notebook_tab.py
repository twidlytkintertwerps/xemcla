#! /usr/bin/env python3
"""Holds the NotebookTab class"""
# imports
import sys
from tkinter import Frame

# constants
# classes


class NotebookTab(Frame):
    """General tkinter tab superclass."""
    def __init__(self, master, root, **tab_options):
        """initializes a notebook tab.
        Upon creation, the tab will be added and rendered to master
        :arg master: ttk.Notebook to add a tab to
        :arg tab_options: see https://docs.python.org/3.1/library/tkinter.ttk.html#tab-options
        """
        super().__init__(master)
        # self.app = root
        self.master = master
        self.root = root
        # add self to master
        # reduces syntax when using NotebookTab in code
        self.master.add(self, **tab_options)
        return

# functions


def main(argv=None):
    """Initiates program."""
    if argv is None:
        argv = sys.argv
    # work
    print(__doc__)
    return 0

# initiate


if __name__ == "__main__":
    sys.exit(main())