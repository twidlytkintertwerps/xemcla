#!/usr/bin/env python3
"""Module that holds the icon image for the gui in base64."""
# imports
import sys


# constants
ICON=icon='''\
R0lGODlhAAEAAef/AGIkPWQmP2ApP2EqQGIrQWMsQmUsQ2YtRGcuRGgvRWkwRmoxR2sySGwzSW00
Sm41S282THA3TXE3TnI4Tm06TXM5T247Tm88T3A9UHE+UXI/UnM/U3RAVHVBVXZCVndDV3hEWHlF
WXpGWntHWnxIW31JXH5KXXpMXX9LXntNXoBMX3xOX4FNYH1PYH5QYX9RYoBSY4FTZIJUZYNUZoRV
Z4VWaIZXaYhYaolZa4pabItbbYxcbodebY1dbohfbo5ecIlgb4phcIticYxjco5kc49ldJBmdZFn
dpJod5NpeJRqeZVrepZse5dtfJhufZlvfpRxfppwf5Vyf5txgJZzgJxygZh0gZl1gpp2g5t3hJx4
hZ15hp56h597iKB8iaF9iqJ+i6N/jKSAjaWBjqaCj6eDkKOFkaiEkamFkqWHkqqGk6aIk6yHlKeJ
lKiKlamLlqqMl6uNmKyOma2Pmq6Qm6+RnbCSnrGTn7KUoLOVobWWoraXo7eYpLiZpbmaprqbp7Wd
p7ucqLaeqLefqb2eqrigqrmhq7qirLujrbykrr2lr76msMCoscGpssKqtMOrtcSstsWtt8auuMev
ucKxuciwusOyusmxu8Szu8qyvMW0vMuzvca1vc20vse2vsi3v8m4wMq5wcy6ws27xM69xc++xtC/
x9HAyNLBydPCytTDy9XEzNfFzdjGztnHz9PJz9rI0dTK0NvJ0tXL0tbM093L09fN1NnP1drQ1tvR
19zS2N3T2d7U2t/V2+DW3eHX3uXY2ePY3+Ta4Oja2+Xb4ebc4unc3Ofd4+je5Onf5erg5+fi4Ovh
6O3i6efk6erk4+jl6u7k6uvm5enm6+3n5uro7O7o5+vp7ezq7u/q6O3r7/Dr6fHs6u/s8fDt8vPu
7PHu8/Lv9PXv7vbw7/Px9ffx8PTy9vXz9/jz8fb0+Pn08vr18/j1+vn2+/z29f339vv4/f749//5
+Pz6/v/6+fr8+f37//v9+v/8+v78//n+///9+//9//z/+/7//P///yH+EUNyZWF0ZWQgd2l0aCBH
SU1QACH5BAEKAP8ALAAAAAAAAQABAAj+AP0JHEiwoMGDCBMqXMiwocOHEAe667WoS5Imcj5Ri8ix
o8ePIEOKHEmyJMF3vw5hgeEhhJAzkXi5M0mzps2bOHOSXLeLTwwMGCxcyABiBBhlOpMqXcq0Kcdu
i5Q8GPAAAoQGCQDQEMXNqdevYMOWZNYlRIQHEdJGgKBgRRxX8sTKnUu3rr9iRBasVbuWQQgnmeLa
HUy4cE1iOQZA4Lu2wYcijwQbnky5csNjRRrsVQthAQgrniRbHk26sjMwJCag5bzgRR1bokvLnj3X
GyQmUxk0eIB2ghJU4JK+i0e7uHGD7n7xIeEAwwULDyaMOHMtZ7533Hz9yoYu3r3j4GX+51unS1AX
IjROoNUhSB3OeMoWjbEy5YseT8jC6ycNv1QfN1iYkEEPgARn0z3JTEIEVggsYAIWlkBD3H4UGvaO
N8kocwsZK3QwRTA3lZOIDhwwwMACDERAghOIIFXhi5OJYgUELkQSTU3yFKMFAgosllYDDHzQBC0w
FkmYNZSkAIETn9R0jShKKJAiZw84KIqRWNYVTBMYjDAHOQLFU04447wDEjKG9KAbYxEMUEFkWcYZ
ljZ/9BCBE7q0U48yrpCiCjLpfBRMHTXsxiYCGRziTWxyNqrTO7fIwYEMfHACiiFvjIFGIJq4wgsy
7jEkDznR8IKIEiEwsBpfCHSwiDn+jsa6VD6p0JBBDDCkMMIHHngAwgo5WKHHLg2V44ogTrgwQQMN
+JiWVQukcKWs1Oo0Cw6aLYAAAgkokAC3D4QARB2siHPQO9TsUsohYwDRwQcvyLDCBFICmUADMZTh
S7X81pSPN5fEoBmbfF0QAhOuGPROMpycYUMGEVDQARJ01MEFCQgw4EADC2BwRi7o9CsySeVUokUI
DlhFMAQOnDiCHvv6A04wpyziBhIrEOWDFndoQgsupMSxBBA9HIGFHKnAOvLSIB3jxLMEM9YABkDw
sY07rvTBhAkQMIBBDV9A4ouZAskTjCiD/AHJLNpMyPTbEN1TjSYzILBq1GmhBYL+EHfoYQUNHYBA
gxV1RNJKMuwUtA41wfiSTDhwRw6RPKmgQcICzuKtFlq8RmDBCEvwMYuBIsljuj32mC750u8g0gMH
zWrOmAMPYOACRpCoEsw4JcUTDCmWVFLKLzOt3i87cXywmeycXTCCE5YkUxM5vSDChRJLgGFILt4Y
z686YRjKPF8OZFDDHPmZJE85n1hxQwohjKCCDUtAso731KKz48Dj/8gBEYkoXknQgQsxNAABDXDA
xhAwAC0Ig2z4a5Q6uIDA/qmFAR0wwiLuZ5JfxKFQC6gKBB7gLSAsIn0RlI083FEOcphDHe/4jkLW
MQbaZY55DPCAEyARqpKwAgj+GVDZ5hzwAjHUIoWyeUc1eqEKUqSCFsVQWkLWMYcRQMeCEVhACMAg
Cg6WxBQr0EzmRjgCJqACiZW5hzeC4QpQLIIPbRgDG+gwiE75AhqM8oc8HnEED8RufBBAAAn4wAu3
kcQULnDA8s7ygDKeEY2TGYcotIADFJBgBCEAQfxIYIIZXOEQ00DILeqggh71DwIDgEEp2JEPmrSC
CB1Y5AMc4IIwzAKShZkHMijRhREwYAAD4JECvMVABIBgCYsQhiHlAQ1PhCEEdrshwZrVASv8wibB
mAMNvFWVHzFACCfE5WDqwYxF0MACE1iLENW5GAhwgAZvYAZBlKEJLaAgi93+lJ0CJoBMbdgkHKzQ
wrfyqZsvEAOC4pTLPZRxKgpEU3PMioAOEHGLZOziEnRwwgo+0AMdKC+B0ryKXmpQiAfaRB7YoIQV
bDCCLDIgAzdARA8TKpZ8ZMMT2MLiAziAAzEsog0sqEAEMqCDPviBCCEYYdRUJYM09EIn3aCFHzRq
AQaggAyqkNw7fOGJRSRiEZNIhfQGEw9QaIEDCJAm3qpyqyTIAAIkWEIcKMELX3QiDkBI6re2tS0H
mKAJfXBFyHRijl6IIhJwcMHzQBG5dRDDDz4IQQY+QAMsTEKKdPFGGSCmVuZZJQMvIIMoskGQYPCB
CTnjS5eyoIlmeEUZWAD+gQkAATd3sKIMOeCAAxYwtRUhQhh1cUculqCAu1mwMwtAgiV0sRGCrMMX
qJhEIvowBznooRCRYEUz8qiTceChBhlAQ1eW5g5hzMECafXRAxqQARs4grtOUUYkdJAA41pQM3WA
r0DuQY5lIKMb8BBLODgRBQhEgRde7Bcz9tADrAjxKi8dQy4wCxZb1AEGxcUiX3RzhvEuJB4IBctE
5CDRSbh2ZLggwgUWmRYTEWERYxVLKS7GgM7icAFZ0MVM00gOR3iABG/IxdJQ4QIERG1jMUiDLubC
iSWE4I8ajsCJnJCKbszGFC/AgBBIMTJ5iCIEA4haVR4Ui7lMoggfSFn+lL3JhNHOJhdIyEAIFtHl
T4BZzBAg81wiQYQ021h2QGJCKbAxm2LE4QUXqEM39JsleZRiBUYmmALzJWS5aEIJIICyhk8UBVaQ
jjTWmMQRHtAFXOxYVrYAAtQYcyIhHAKFYSGFFkigaSwyQAFaQPBs1KELMUCACI4gbb+KQQYYUEBV
PhohAy7whVrwTi60kAMMQrjmLC5ADNFo5WzCwYcMqIAMrOiGNaaxjVNn6RqReFoC1KzsCchgESEG
SzIgkQO7VXs3cEiwbCZBgvbKYRKPgMQnChmraSACCUPpdwKdh4Q98KIu66BFEjKs4atkgAaJ0DZt
OuGCCHSgB1ZwQhT+tDCHSNjiGvaQUzC4sFGQzyADoHWCI7pnF22IgbMartIK0MAK45BjES9I5wQy
ABSYk6ALoNiGnFiRgxAQAQ+iWMQe+JAIUBCDMO+4RBQykNbj8uYIbp6N7wzhBLM84EQLWIACGoQC
LViCGfN4yDqS4QpPRGIRYEVFjOvCX0h8gARzqHQ+tM1or8jDGpqgQQL+zJgHXIAEcPBwaeBjCBJM
aakYcMIjhM2QfAjDEV2oQQgwoKImQAIbhWfKOmZxhgf0oBSfpow8kNEHIQQyn2Ju2Qrk4IqUz2Ya
giBCIG2Mos/MovDdOEUdcIYBBmyrkUiow5Lrwg1B9AACWghlaeT+cQxDrMC+bIKAwXKscdLI4x6u
8IFeGB8BBMw5GoymRRfCyD91LqADgMCGDOXCjCx84AR6IED8EQyJYAWpwi26QTu3xgAUAAJAsAeC
RRvzEAyDYAJdxzyIIgaooF/bkAgvUCVQNkJ2gwSLQGhzcQs1QAFLoAnFIQ/coAlO8AIdAHPOMRQZ
QAEUUEQgUhzrwAlaAAJ6MT7sBQR64FrrsA7ukITu4B3YcApfEAEJIGYetwTEIhbyEB8j8DK6kHqE
IQ/ZcAuZwAdhoAQ5AAMrAARcUAMPUAN1IE/FUQ578DD1pzkQYAFOpweOsAiOEAmWwAmfIAqpEAlk
cANdEzWLsQD+MGAK5ecV70AKX5ABOGAKNHcc73AMpWAIc0AG9vEJXoABMxAHsCYb4VAGscR+jNEB
NQAERHAES+AEV6AFXlAGWCADpag5AwACk/ANYuEOevACFuAELhIe7uAN0JAMxCAM1BAOiyADH4AE
VUgb4KAFzWKKm8Msu7EYE4ABMDeD6YSBILAIbggW1qAFFkACcnAOCvEO3aAMxUAMx+AM3iCAlJEK
TpABLpBVxeENV0BtWHQBIDADOFADMyADMoArL+AC/WYB3rgIzxAWWCMEF2AFmiCPBBEPxfAIXXAE
RLAEZRAJ1zQauRAHJAACkaAOizga4OAFq8Y80TECWKAKxHD+C7XACqlgCqIACqDAB0jFYoxxi56g
b+8wDt3QDWUyEu6QDcKQC7ZgC7kgDMjQCn+wAiDwB8EQdwfBDrxAdiDAAIuHAlGQCMgQb4SRDI/w
MHpQDGJZGeQABydwAeA3TRhgA3oQKAKRD/GwDubgQrcQBzggZZqjAC+AjwPxDsjACp/wCYczEsQQ
CV6gAzEQAz2QBXAABkWAATLACr5nEOuAC3cwLxagMhaAAT3QB884GeTgCkgQAV9wChQ2GuhwCEQA
O/2zABnABIsAJgSRDzKEDaCQBRAQhQSDFhyQBM+YI53AB2fABV2QBoIgCgfVEffQDKbAB1ZgAr80
AA0QAjb+AAMh8ABAEDMH0Qx8oH4PdRUIoGWOYG6D0Qxa8GuJMIml8Q6s4AYgcIGykwAgEAe1oG8E
cYV+QGsgRSXFRQSFoH3y8AyGQIscAHNEIQNtyIUDIQ/fsAg8IFkKCZpB8ZlEcAsJQQsNxmJX8QBi
gAwQ+hXv0AYRgAJtUA20cQ/h8AkvoBjjkwAr0AnmwGiqwAQfgCLOMkIJkAF68AyCMQ2LgAQPwC3D
tC0NAE7FEBHHEBUW0C1qthbrdY0xQAevwJ/r8AkkIABqtQADoASnYGWVEQg50wR7JxvBAAYnEAFz
yBdXAQEhYAUPxxDTMAlk0AMk0HzPJy5pUAuS0Qo6sJL+VPpxlgAR6xAJHceTcGoBHQAGaWoPxTAI
GBM1QCIEidCklHEPntAEEVADpSkbkvSICoA3hdgDRegQ8ZALg1AFMZABzAICScCQbhMf9XlDypYB
fsCF48AKYtB+jDdCCoADjyA98VANyOAKiIAFHhCEbLIxPUCVlZEPrvAGHYACojAOJVoX3mAJOnAB
B8Qjabd25ZMBHrACUVAKkFMsvlAKjjAIe6AHh/AJV0cQpcAGHlCqBKMADdAG8NcQyRAHNmABb7oy
DTACUhAJ0CAMUQcHScACGPCWl7oIx2AZT+oCIeAHvrCtdbELVtBSuBoCOUAEPpABHVAGqYCOERoS
jqD+BRngrKzWAEVQCKWwC4+jELpQBEJFjbWTAkcwBlpwBDegApK1XlGjLUpgCv4ke+jACjhwAWBg
CmlZGeEwCTUwAjoQBnowH1KQBWXwB59wCpWwBPbIBaLAC7uQtshAOifJEIgwBRlweYxhFSMABF/A
B9n1OOsQD/AwD6kjEKywAmFWceu1GBbAAY/5ASsmTQkEBsfAsV6RDEfwaxtEG+RQCl5wAS/Ae9hQ
C5ZACaKACyb4DosABBMAAl+wB35gVIZTDZkZEZPgBS9riMwCAfCSA0ZgBWEQB4IwCaaAC8wADucA
CiMwuMelSCCgA1kQB4hQCpPQBTDgfN1knqK5CIP+ZRnd8IgjkG+0IQxh0FJaoAvncA/jAA3RkA3l
oHHR8AlOYAIxUAM5EL9C4AQwsa4RwQp08AH6SjAY4AEjAAJEJ2UQ4AEzsARjoAeUcAqssAchAJxe
F1d3IArAEDJbygUhQAGLmxYWkAN6UGmuiQcvAAFWQKakcQ/bYAkrsAAzQGcMYQ57UANRaiJcuQAX
0AR1GhHfMAnQhKteIwNs0FVz8AVKwFIYsBgd0Ek6AAQyELE51wA5AIHe8A6C4Q2mUAZEsAIRUFxx
JQjCQJGTsQ6QoAQRUAS98LqVIQ+QcAQMsAKHUK8LcQ56QAMXwK8KZCIKEAMwxrG5wAQeEAE2NEL+
aCEDZdAKLqgL7qoHYsAEOvACJBACHOCWbwloC3AEn8B5A+ENpfAHYpAEPVAEX7AIoWoZ8nkG7SUK
Jjgax5AFQOFAqScOcGACbgmn+FkGqODFC/ENM9Z+3ZIAA8AAJHAGOqZH6RAO2FCMSZkKlAAIbnAF
PEACV4RFHKMEpbC0BGEP4TANyBAMvfALx3AN/HnGxWAIIWACftALkOsUwlAIMIABTPAIrYkQ2mAF
NXZD97cE6dkR4HAJTEADZrgCMUAEosVo3BAMrKAJgpAFHQCz43NrU3AL11sk8oAOorACHPAFj1QZ
iZoEGbACJdW2B5ENS0AVbNIA/2MID/0Q+cD+DbrACX4wB3XwCKzADOnAaPLwDupQDsMLaRq2AAkg
BpKHJbpQAxAABJFgGfFADGCgjVYAng2hDVcwjazmAfasng5hDbvQCrSgDN/MEMJAtmfRP3qjB4aE
JcnAJSPQB5ZBDAm6AEbgCCrrEOEgB1hcY5yBAB4wBqVgyw4hD/HgDlLs0QsBDXwgBH7pWQwAAkyg
CecsG9MQBy7wAGgwtXOhD5EwAwwwAo/QDYB9EOqgCVjAdT06ACOACCQqF+hAC2yQAYv3ZyO0ADeQ
CMDVKOPwCEkAAT3gCJ8ACqfgCrhQDIsyF+dAC1zwACDABTf8EPLweUgAAsiFAA5AAlowC1v+7RTy
gMIiRIcRgAFaEIpZog6lEAaw2gEcAAI1sARf4AeqoH1i0QtjQAIRoAWlAJ8QkQyJUHZnhyhjYAr2
OxfCoAdEELdSwhsCrkDiBwNjIAp6/SLe0ApyAETbEkwRAAIvQARdMAiuIN9MkQ/f8AgzgAEvgAj6
5xEoIQp/0AZfcMCqUA2LjSPRUAg3kAG4tzlDxQS2UA+O4g6p0AYooEgr8wC1lAor/hHjYAqZOwJe
YAul44W/QA1SXBjJPQlyIAU5oAKXNAL+PAIQgwQ7KCfdcFsykEVzO0vaEgEwEAaeYMk2sQ7iwA3h
IBjCUAYm8ABOcAqxBxIJXhfH8Al0oAX+ULAETKAFYbAEX24DqtAOcpIPrNAGK0Bxa0XcStBzOOIP
6sAMvOAKrBALxEAN2+AJi54ChxDkxfEO01DprFDqtGALonAH/eYGcJElNtWLWcR4VUoCfpAMko3P
rFAIY3AFVgAFVsAFZPAGTgABKzAsq+MOtDC5NKAHdV4h4eAKTdAgFlSqEknNIvEO1qAKZ0ADgZR2
aVc+F7AATiATxrMNc+ACGdAEunDrx7ELdTAD93JfNEIGFTsS4UALdEAE6L6AaLcAtBMBZ7DfkeNl
XHABK5AIbgwjotBnMS477dQDHFo6uIAHgsuPcwsBGRAGxADqReINMOqdj7DZx7EIfrT+ZgiwAqQg
8g0hDnyAxSGkVioDTxFvPMngshEgBtBgJH3gfGs2AHNWDeyuEAQUBQOwv4boeDAQQN4DDukWATng
CGkaHilNBw/gwFiEn3UwC9XQDn7Lsb6gBzfAW57VABYwBrvw1gN/DIIQAh9wBabwIvIQDXJQ9Wv2
UkswB4tgKaIgCqpAC7mwC7/glMoQDdagDeBQDuvwDr6HCpjW8EebXAqLP65ABBHwAX3wzsaRD9cw
B3RfbUNRFCQQ+v7cA0hwBWHwBngwCI7gCacwC46jDa1kCSPg+Ee2hnBw3JIzDXygeExgCs1uHOWg
B52vYQ5gATCAAzAwkkTHMkPVASP+4AI2IARKcAVfkAZzsAeGMAmiAAaFPT5VcQJXcEveU4lzzwHh
+yKDkMVrlgAfcAefkAh4gAZdEAVFgAMvEAIEG6ARkI0wxwEe8AEAkSHCQIIFDQ6EAAFEElb+HD6E
GFHiRIoVLV7EmBFjPU42LriIhE7jSJIlMVqSceEBhIMtEUbAAKTUNmG3WJX6FGnRID1xzoTRYoWJ
ESA4ZqwY4eFChJUuWyYEgaShSapVrV59yCuOiwtlXJnDGlasP1dgVjR44PRggwg13hxzeC+fPHn2
6NJ1B+7ZL1qnODkadCeNFycxIKRVW3AliSquxj6GjJVcKysPZNBhFlmzRmqglDT+cJC4IAIIXlCV
0/jOnLZmxXzhinWzjAeWogc6eDCDja7NvX1XjPcnRIMcjn8fh8hNTocItUUvALEIbNhPNzIktI22
yKJkyL37ZgXGAwc8veR9/50vFZkVCxiEdgkhAQIYbHKNxSVGxQL4iRc8EKOXddAj8DFrRBEiAiIO
AafAzeQ5ZpEbFIhgAqcogCADMWohZyxlIjnigf7iY+mEQ85zMMWroDnjAw6sCEbFyEBxgoPcWFBg
gAQUWGCBHBG4oIc6WCEnn7HkSUaOESBgwLmDGBjAhTp4k7HKkuSJxIgHXKhkGiOttEoeatjgIAMa
zvjChQ9A8KADDjwAwQUi9Pj+ZcDIRLHChIScJAgCCjLoIpdzwCQ0I2YcMcGBJTT5stCSiPFjhgiS
GEQXXz4xZI4xtMBCDENEoSUZOyMDhxUtQGAAgfcccAA0EWUYcjpHZ51IGCci4OCMaxyaB51wutlG
G2/GWQdFWv15Z5IeMiChD2EcigcaW0rRJBJSnLnnuGSxkMHFDL7NAAMLGCiCFmOPRdcfbwoRwgIh
QPHGn2hMiaQQPfRA5BNcwkHXHFe+YFYLWhqNx9dutBEnHu/A0WURMYigQQYaaoBhBAaQaCZdjf2J
J5c6QCChjEtseeQMK4zIQYckvMDDk12mGZXQXuaQIYImKomX0GJE+YMOOer+oEMLHR4oApqNNZaH
FSA4WIEGHWIY4YMOvu0A5BeK+EOXc6s0B5IXIGBhEdQKfaecbrLJJhxoLNECAx12OVpjZbQIAQIF
FNjRvQYaYADvASCYs5acqwynFC0aIMENX462JY4OZkhFpLiPhQaOGSZoYE8n9xxoBS46ecdKXqQQ
aAxiQtf4nl78GIHLabae3Ep0WLmCBKYSk68BFMBgpcEU7xGmDxAwWOKTeo6+hxlIViDBEGEajd1K
d1iBgwQGbEPIgd1LydZBcvagoYEeQJkm7nzCISWGD+ig5fjowZymjhosWAD75upf4Y5sHnMHGVf+
WkQAF+EJWrzOIepQxRH+MrCCOGTGfP7AhQ0wEIZTwO59BYqHLYiQAD5lRwFMcMU4wiKPdgjDEV2Q
AQcGYgEMnOkT1PDHPVwBhxB8gAyomEf0ktGDCDghEzm8oIryIQxEvGAAiLFfBBqAABr0YXFXGQcu
DjGGJLjAAjkaAAKANAMnvCESomDDDTpAhFIMbnLTWEJMFqGwIKZIHqLQgsU6KJrDjIAJorjKOmhx
h/Y05SBMcgAO8hSBGeDhGxfMBhdAAIM9oK6NGESEDjgwIvthJwWLsAo5ZoGGmiXgPXyCgAPwxiwG
eEAPvrDgxrrxhhVwoA1sfCSB3lGHEFhgjthbAAT8UBV52OIOKTiibQ7+M4AVfEId0JscOPpwgwd0
IWax9I472MCUW9rGk3ugSj6gcQcYRCCXwnxAAkLQBlY4MnbjcESIrLA/aKLHHWRYAFuSaBAFNIAN
zTjHOtwRD3tkRBumKMIArpfEBmTABnfYxgXRAQotRGAJwkhlOzXzznjO0yBoIcIeIlEKWhBjG7Cs
iCvCgIL/zJMlEGDCLuYSvXWwAg0YIIIrOiRR5LgjDdS0KEEe8IAQ5MAJYqiDISyRCl0IIxnQqAY3
woEOd6DIETDAAHbm+YAByKAS0Oje5N7RCz1wIAefKB9NtUUHEDQnpwjhW2gyALIVyOAGRcACGvSw
iE+0IhjZUEc36jD+rrPaLWSoiOix5AGNRHwgBosohliPE49CWIeSlcRACGYAhBzEIAUhEAgDHsCs
GyRBC2zgwyIsAYkm9KivDeiAENYYvXykwxMhIAEeqKTY3sjjE1iQo0XryIRF6IIVkfgDHLiQBBuM
QImZu0AGPgBbEnBAqjl9wARGcAdzTo4VzSVDK2jrG3n04g8uQAAS7eeABNBAD7zwxzqUwQtWgOIR
g5iDGLDABCLoQAYpAMEF3nPWPqUFDe64YC5kkAEnkGK7vnGHK3QQzHmmSio5s8c84uGOdaxDHeXI
BjFmAYpF4MEMSnCBQKrpwTEA+H29IMIHgFCJA/umGWlIgUoqyZb+ELzBaBgxxzSEgQtWLIILhhGv
RR3AADKYWIdWAMEKMNnizaiDFGEAAYWwB4EFhMAKnghsRFCkDEck4QEDzWlCKuAGI8dOGKfKwB0G
xeTI5KMbloiBqjR7y8Pw6AJHsIQDSzKOU1yhAfU7a3RHUIfqagwcu8jEG3AgECHoIRKsGBubx0IL
HABaNJvNgR6wURV05AIMDrC0kDmgg0OAFGm0wMMMVsIStqzgC/eRdFjyEQw+hKADSfABCB6gRR71
aD4PGMER3jAJVFqlG3JogJR1q4ARiMEUWSaUOngxh/klwAEsWQBpYICGUpQ51hChi13CDTtvDAII
QUIEHpawAnD+hQsDyX2BExLBi0KbBBG6HrFT5EODRRwDmej6RSFsgAB5FiQ0IegCMr4tkXhooxi5
cAUrXIGLY8iKY7hoQgdY8AZiKKMVjqgDG8BwBSc4AQx6mMQslCE5rKQiCyQoaRIT4oAk5KKfR2uH
I5algCAzRUQ26PfNJS2PdZQDGrkghSP4MAc4yEEPjmDFMchBl2AcwgQY0AIeHUIOYdSiFJNwxCJK
cYx6WyUZkSACWubpAAy8QA5mTNdgz+AjfSsABFr4hLdbjAxQ1EELSaAsDWIAgxjMAAdE0IIeblGN
QvDAATMQhTgeko91jKMb1oAGNLqhd6wg6QwZeEDm7IcAD9D+4RZln9U3aGdapxT0Bn94Jm3tEY5e
OMILX+N1j3SPgAFYgAZ44MQSIjACMrDTQZRYgu0Oc+kIfIAJrUD9rJSHhL2phSUfYAPLD6yNRTCh
BiOI6t52On4RZS4DLpAgCN7QCs57BxulyEIIUuXHP8onA22Ihe/iRgxAACH01g89L7A42nIGTVAC
u+G5EaszBBAAEnCEavg39AgHSvgCHQC/0EOL8RO/CECADOiDZoC2QuG/Hvg/tXiABeCCARQrd1iE
GbiOVaOjw0AAEkgEZRA6B7kHdSAGTWCPXUOAXlMALXKADHCAC8g6FUwXZniEI6g+fYuACyCDSFOs
cWAFLAj+wnxriQX4AC8ABSQsEGooBTrIAiPoARuQgRnIASJgAi4IgxvAAN3YhRAEk3GIhSxgPZdo
AAyQAT1QhwPDhSzQk54TpgjIACxILCuxB3cYh2RwBUuwl6dDhWHgBmjAgxFoABc4hGOQQysZhzjo
m7q7u7yjLXlQh+XhHOgagBmwBAOKtmboBVvAhWLQP1pIgxBwgCNYhGqIHUsQggxIgJ7bKQaggUQg
hk2cHHSoBTbIgFDTLQYYgS4QBWM8jnsYN2ORh2HggghgACJQBSnUmAjBAYI7iLTwgCtQqQiMpWkw
BCPwJv4aiAfAABfYg/ajlXcABSx4wi8ohdg7FncQBj/+KAJd+7Ig5CAXIANQ4JftCoYsMC5BnKcG
SIAukLxjZAUhWAAL8IJikEY3OrscaJKEYIsUEJiNnBxayIG0wELRgBIm0AV6pBVsWAQjWIARmANY
Oxp5uIUeYAASIAIxYAM5SAQiabF8KAUSGAB3NDgFIAJOiIbYsQdocIQVYDY+SMiJIMne6AUdgAAj
4ANbOIZmcElougdymIQROEqkfMcFyIFDiJHYuYdfoIMaaABS0zMt4wZt+AZ+/I588AZRiAEPcINU
CAe6YDN5aIZEGAEEQMt3FEbZep91yIU48IAI6IFPsMZ3WAdv8AVUMAVVuAWPMod0eIerpIp72IU9
GIH+FYCEZ/i2dxAG4VDMxfyyGHiDWbigdUAFhsyALBAFO4mGUvgDMXCCIzACJGACJ8gCNPgDUPgF
U/sNVOACDLgBc2nNYACEEIhNtPwyGHAD27wgbLCEJqgAE/ACXCiHYLAEMrAB+UiA9tQiBMAAG+iC
Q9AFdEBHrHgHctiGafAGZfCDHthKZVi4eEgGQ0jMxWSKxpyt6JEHb1iEHvgAFDgDPZCCG0AB0Nub
vWGADc2QEagBLDiEuhyLd/CFRSCDJVCCJOgWrOOGhasHbYCEA5XNBcCBQviFRxIGPZiBBoCBGoAA
BPiPOTqMHcmAInCEYuhDsZCHbHCFP0gCEBgAAdD+kc1yAlYYh6wqTFEwSwR9AAUAgkdQuEeqhjGI
ruu4tmo6jLToABgQg1tgB7E4uyRICgvgG7RIiBFIAkfwRiZzBRlQNqRcoiNIBbhjLWYQgxMUPewJ
PQQwATy4BSCyCmyIhCmYgADQLIPYqQtwAlGwhm/jhSVQoZRMjCWyAhF9n12oAxvADVHtrwswgTnQ
P6rghlNgAluiv4JICBW7hG9TBjrAAW1EypXIADfwwriJh0mQpFudMQUwAlSI1ZKohTcwysfqr4uU
g2rA0gMDh1LwgghIAHdkkjFahOjTmHdAhjiYAGY0qQZYATFQBas4hBfAAGV9kgewglLQhqGjhkL+
sMTnmioFiAFA4IVsjR1r0ITS+lPdgscYGIQ9zYhwcAMIKDi1YADx+YNDlDRWOIIPWD7dGggnuNFH
2oUxgIGOdceK/QJgCEuJcE0ucA/b2Ck2xYVvs4ZJcAI6dci6e5FJcNjYQQUdwACzQspsQwJPCKuR
CIdYiAIFADMThIARaALt+jYWjIGoYtXayAFQIAeCjZ18mAQPyE6kZJUbqAP0IgltOIXSmlh9g4AP
OIJ3XbhiWAQisJsgjQ8R+TIOyIJJuLE2sgdtGIQMCFt33CkTwIKpGAl3wAUseNlLe1onMI6FCwc+
mAEPsIDEsIDMrTLpuM+4WYdfqANfRNCE4AD+IDAwjJCHuZAHcrgFxm1ap4jZMZjZhXOIYuAEOUCC
4eC99+S9IyKBF1iBCOiARUjSR0IHWnAD0V3MhLAAGfgEjHgHbkgGXmiFT/jPihIN0MCBOwhZ2vWH
ecAFQ7ACGiABEAiB8x0BEiCBHNACMdCCFbiAOPCFldWYcmiFz0tY/tqTFNhVf5AHeKAwdDCHcfAG
a0CGaYEEP3iDLkgCFGgOLMwcJrCEvvVec2gGXkiFSEAEQRAEQ3CETCiFWeiFYJgFLGgAG9CDXXmk
cogFNMiA/O2rwzgBTfAHd1gvVviEReCDNyCMIihDGCABF+EAEPgAbcy3zZoDCPTeiSAHaED+hmNQ
Bm4wJ3v4g/wygtltI3WITOUdXRDAg1w4BaVb4CboARkYAYHIAA+ArbbqASe4ggE7jA46jFbpAEAg
TbGixnd4B36SiFK4Ag8wgQe8weh5h2PQA8FF0JcgARoAYhDogA8gARnogSNwAi94g9DyhFSwhV5Y
rzvQAQ5U1hPUIgx4g+5YYpNAhha0AH3Uy6PhS0RA5ETGgEcOARN4gRnQASOwAjKog0H4oljwhWgY
wD0SghCY1wzVrAwwARx4gQxIgkTYhTU7ZY2QB2SwAmd0A0KNHksA29F9WiIog0L4Ilr4haOahmzo
hnAoB32ah2xFEksQAxfwQQZkgBngNkH+cAEMWAE2YIVWnmaW1YMVAJxbuOMqSQUcsBBWbT0G8AE+
MIVggIYiGQljaQZS8LslKAIiMAIs0INTIIdekAMaIKQ0QIV8/WfURQUy8IAUEARf6FyNGdmSzVn7
8ZEvKAZ2oAu5oAp5UERo+IWI4wVoEAd3mAttSIRF+wAneATtO2mKoAZNuJwkiISCVhFoiAQlYAB1
lbkHsIAVGAR4iAx1uFKJIAZCIIIOsCNLEFB/OAdm4AVcyIW7amqHQAYpgAALKIOqfB9/RIMuJVwG
QAExSAWqhggjWSmJOIdIYIIOmIAcCIRwYIddcIQ28IIwqANR6FnaHYc/wAEGEAJVECH+aPkGa1CG
YgiGYCAGaCDXFEkHSujFmDOpEyQCUOhUAmGGSsACduuBQjiENPC+F4CBHtACRFCpk3YHWliOxKEF
aDmGVxCFSUCEQPiDQ+AEY1htBxGGQvCB/fLYDkgDky6QdTAFMggBYCviCKAAhKgAEqgDbV64fDAH
UJgB6RwSTdCDMwgKjBYCISCCJiADPpiEVhCVQvncOhBpDkpvmIXIETiDVBhk9OiGUwCDbmrPeGqV
rG4APHOGprYVgciBCrQAvCW/YMyAHiiDSOiFNy0UV3ADGLi2SrKADGACWqBf3yCHSriCZRzHBhiB
e21qZQLQ8+MAH+GRDTXyBUgA1Ir+gSLogj1wBX9GD9bdAyTwgHhKVIQov2xTTBp4g1J4Vu9ZhCXQ
cYOIYxJYsn92hkRIowSkoz+DABKAg1yYKSu5h1mIA5i7gKV44Nq4AAz4FhDIgTTIBcLGimrAA6Bd
W4JIcmyaZnnwBUQ4AuNaVToasgawABfAglKQ5ipRBTLoAAuoLAzIkQAIrw54AR9oAjQwhFL4Baau
6jSAgQugVg5cADpw9VgzzENAggv41jBTzDKYBb1OEXJYhCTggBiIgzRIghsAXhkAAigYgztYBFbw
EkeBBliXdZdQAAeog9D23mBIBCIw09N6cyxwBULf6YXkgGbzBWh4a1qgBVuwFGL+SAZqmLpZuYY9
6AH9con62aUlHgdIEHPSQMof/QA6cc7HsId4eId5gJ1kcAR5doJOCKJwAAUueGFQykMbsIQlLgdb
0ALQm2n7YQAMwAE8cO+wgIduYAZkiIZCiwQgcAAX+IQvjxt5qIZEwM5bhZIYOK8l9oU+kAFxXN6B
UAK4EYt1sIZieGtWEIVISIRDWARQaIVegAZzeIdmIAMM8AAtMOU2Avl85hsjjwATIANaEPZYm4RF
e/HRVYAZcIRkKOh86IVHKAMksAHgNQH1VV8bsIJAmIVY0DkLsIJKSPvoKQddqIMVcADedQE6eIV0
WOJwqAMMSPSCFycwIIWEp+b+ZCAFPCgMDEiAAcii9xwAuzOCN0iDKDiBFTgEZUD3FHmHWbgDLnCC
JGCCK8CDWTikCraFLviyRNapDPB5FTcJMfEDGzBmPdcczonxqVmrJbgFmiJFn56FW4gGdIiH2I+b
ZogEJNhQ4UcIlgiDiTQJXxAEIoiAu7nycWSA+VgAGX8isbIHdIByNvMFOmCmWQdUBIiCjAEIfwIH
EiwoUF63PyMgRGjo8CHEhgwrADEVzyDGjBo3cuzYMZ/HkCJHkizpbxYWExAeRGzp8mWEBgiMzEJH
shmiIg0UMITp0gSYTzZNEi1q9CjSpCFPAckAoafPqBEfJOgxiRnJUjYkSo3+yDCDk2JKx5Ita7as
KBhcu7KN8EABDkTCRK6jBacDz7YPVyaYUakayLOCBxMu7O/TiAZ62z5YUMMPL5HQ7ui4wGDxXgUj
yJh6Z/gz6NAkM3lQgLlr4xl6IofkpQSD29MOG2CYsYed6Ny6d/vL9MG0bJ8PGMSI4+riPXkb5Zly
MQCq7KcPuGDjbf26YFArWEIPDhFCAxNaHPmaJu7exmSHSCDw7pAqElu4sdOvX7TUDQxP3bdc+aHH
F4FEIsosvyDTDDXXcAOOOfKAc0oaIQDn3gMI+MAJNPZpuKFHqhThwX789fdABiOgAMMMOhDhRBhy
AOIIKLS4IogTHlzG31v+NsgVGIc9+ugPLV2ssJKI30EAAg44xLDCCB5k0MEIMeRQhBNcwBGHEzNk
wBKOC8igRy7K/TimhsL0AcQDihX53gJMuNILKZEYUscZWjAhRA0odGBBBn3CViQEC7zghitkGlqf
NqU4scACa3L1QRyejQNNMLag4okjgMxBBhZKFJHDCBmsCQEDLqTRyqGpWicPM2UokICjMXlwxCLu
+HOPPfK848466JgzDjjdVJOMKmLEgAGX7gUKgxy1qPqsbu4csoIFD3QXHAQIkIAHLmJ2pM0gRHCg
JoVegukttOkWxooYKzCQrLID2MCKrSGhUwkWHzSKowI2HBIMuuoKbFb+NaAc0QC8wTEAQQplKDPS
O628EQKsOCIABCjWDLyxYNq88UEE12KmQAdgiFLOSPIUY4gJ7fHnAAJJ6HIRxzWPZQ8rbrgrMlsN
NNABEp5gw2NI7ZDiHH9PQaAFNTY7rdQ6rQCRAM9d0UYEIBma1AsQilWNGgYq1KHO02Ub9Y4rQAyA
AAILJBzVAghkQMQixwQs2Rxb3Xhath54IQrNZgsuUjzELFLDz05OEGJXc+9xjFHmpEJGB/vKtgAN
kThz9+CdGyTPMXjckIEMffThBHttOwDdSg40sEACbcvgxifIeFbUPeRMIsNKb0v1wAUrjOEL0Z4b
T5A9xThywwIptIH+TDKLaAFDCB1gQEHvDE2AwZMfkGBDG7OYo5QvY8hwgbVtPbVABkYcMs7x8RMk
DzSB1PDACH8E44481fhSih6aAIMLICAAA4BVCGLQAyaIQRCqQEaDoNaLOqTgLQ7oWQIGgAEsZGIo
8jvePZIxCSJoJgy/KIg9dLGINkhBCDhQURO68AY9LEIUczmLK8aQAxC45QHWYp10XMcBPYFACYbY
xgePl49sLMIGJKrDL9ZhkHVwAxrIIEYwhEEMZCgjGtXQBjnqZRZ3IOMRWAgBAhKwAITtZyUNYMAC
BjCAHtSBCzf4QBJYk8TONWMSTFjAB7TgrB+JkAxEsMEKQCCqNEX+IAMeGMEKZkAEP9SCE2nIAAgW
wQzO7ZFj4fBEDh6AgTP04hxjkoc5oHELR6DhCDEAAak4EAMiXKENjtCFNtwhDlXgwAFGeETgOnko
eahDHevg3DU8kQUHZEALp7BHquZxDFQsYg9xGAMY3LCHRFwiFchAj0CgAYYPgEAMzRCmqqjhi14Q
gxpSJAg5TEEEhmiBF2RTla7WoQ50lMMc5jCmO94BzYGcAxJLuEAPSqENdP7oHclgBSSsGYc56OER
sZiGcsDxCS5Y4AJYAIUYBycPYggCBB0gAysY6qPoOSEEGLgABiYQgRVogRPZCIctnJAmJdTClPHL
RddG0AdOqnT+N+VIhR6UMIK4sQ0Bz1mBFe6whzLAMgqTAMcHrTGHGEDACri4Z1GxowstkCA2LuGe
qBJqjuJ5Dh2sIAMGYqAHXwxETEQNq1neYYs7wGA45HLIShigAARYIAmI0NoH5UGOSJjgAkD4RD7k
oQ1i5MIWwfDGXfGqlG3c4QaWiQoCOlAHXoT0g7pgwvXmUAxfnGIReqhDIljBjGBq1iy+aELIfAcR
BWQgDKUYXye7kYkmPAAGWbCCEoBwgxr0YAllWIRYaluWfJTDFDN4jlQYMLc/VKeT7zjGHCJwwAQo
YAEMEGwCMiCERSijtNI1ij2EcQgXIOBr75kACcCAlU6uYxf+bYhA287bAAe4bmEfwEEaevFepczD
FXNAQV6iwhAIIAFynYzGI3C7t+9Q5QSISEZmF9yReKgiDSRYgH0Bi4Ac3DCJ+lAFEURVNVJloAd9
CIeIjUJiE6O4KxAYwA1aLL94HKMPGRiAhBnCBGLkuCgNfnCEpZIAIDA5id1YxGssBxMIJAAHpSBH
iJtskPgagr4pDlkjrZCMPTYjDCSollSy5YK6DVTMIqGudbEblQZcoAZymMYekZGECcdZASigAy3C
bOeC+MIJFiCSTxAwAS2IAn5JJIYO9CzhBZAADaqg7aK/pYccYGDDLgktIqyhaKcJ4waa9kmgSHAG
VIA61Bv+ecct8PACv3qFKgtYARhsAc4kFqMHLiu0CeDQilrbeiO6yEJZdbuACYyhFZbeYzKcAJsz
c9kFfdjFqpvtD3OgAg9JWGpT2dYAEAjBDagAR7idRg04yGACf33Jj2XwiW4MW9whQcYinBCqC1Tg
AhEYARIKIYx3ChMckbBCBrSM7wUQQY/+Fsk7kMGKSEh0DndYBCqQwXBhxgMZf/BAfb9GtRnQoWkX
J8o01hkMZQAXr6wQgoxd8pQGjAANpwDry0lCTGPGg60M3QYotEDABVwQsDBDwArCwAowB73qGVlH
J4yQyD81hE8ccIEYQEEOq5PdIPnoxi0WAYYZbPsB/yEeQyR2geOy070gx5iEGIjggxzkQAlt+EQz
wBlv4wUEADs=
'''
# classes
# functions


def main(argv=None):
    """Initiates program."""
    if argv is None:
        argv = sys.argv
    # work
    print(__doc__)
    return 0


# initiate


if __name__ == "__main__":
    sys.exit(main())
