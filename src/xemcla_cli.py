#! /usr/bin/env python3
"""Takes an EM image and corresponding EDX data and performs the following actions:
-preprocesses the element data(csv's)
-clusters the pixels of the image based on chemical composition(preprocessed element data)
-generates an image, showing the clusterig
-extracts "blobs"(group of contiguous pixels with the same cluster label)
-calculates grayscale cooccurance matrix and corresponding statistics for every blob.

# LICENSE

Copyright (c) [2018] [Casper Peters and Johan Schneiders]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""
__author__ = "Casper Peters & Johan Schneiders"
__copyright__ = "Open source, no copyright."
__credits__ = ["Casper Peters", "Johan Schneiders"]
__version__ = "0.9"
__maintainer__ = "Casper Peters & Johan Schneiders"
__status__ = "Prototype"
# imports
import argparse
import os.path
import sys

import numpy as np
from imageio import imread, imwrite

from xemcla_resources.image_processing import filtering, object_extraction
from xemcla_resources.statistics import glcm_calc
from xemcla_resources.statistics.clustering import auto_kmeans


# constants
# classes
# functions


def float_range(start, end):
    """
        Function factory. Resulting function
        returns float value of string if it can be converted to
        a float between start and end.
        Raises ValueError if conditions not met.
    """
    # depends on closure
    def validate_arg(arg):
        f = float(arg)
        if start < f < end:
            return f
        else:
            raise ValueError("float not within range {}, {}: {}".format(start, end, f))
    return validate_arg


def parse_par(par):
    """Parses a filtering parameter from command-line."""
    key, value = par.split("=")
    return key, float(value)


def filtering_instruction(arg):
    """
    Parses commandline filtering instructions.
    Syntax for filtering instruction:
    "filter_name par=val par=val" . valid filter_names are: "
    """
    args = arg.strip().split()
    if len(args) < 2 or args[0] not in filtering.FILTERS \
            or not all("=" in par and "" not in par.split("=") for par in args[1:]):
        raise ValueError()
    # ToDo validate filter parameters
    return args[0], dict([parse_par(par) for par in args[1:]])


def file_path(arg):
    """Test is arg is a path to an existing, readable file."""
    if not os.path.exists(arg):
        raise argparse.ArgumentTypeError("given path does not exist: " + arg)
    elif not os.path.isfile(arg):
        raise argparse.ArgumentTypeError("given path is not a file: " + arg)
    return os.path.realpath(arg)


def parse_args():
    """Argparse for the entire program."""
    parser = argparse.ArgumentParser(description=__doc__)
    # general arguments
    parser.add_argument("--verbose", action="store_true",
                        help="Flag indicating that extra progress information should be printed.")
    # arguments for input and output
    io_args = parser.add_argument_group("input options")
    io_args.add_argument("-i", "--image", required=True, type=file_path,
                         help="Path to original EM image.")
    io_args.add_argument("-f", "--files",
                         help="Paths to csv files containing edx data.",
                         required=True, nargs="+", type=file_path)
    io_args.add_argument("--outdir", type=str, default=".",
                         help="Directory in which to place all output files. Will be created if doesn't exist.")
    # arguments regarding preprocessing of the edx data
    prep_args = parser.add_argument_group("preprocessing arguments")
    prep_args.add_argument("-o", "--order", nargs="*", type=filtering_instruction,
                           default={},
                           help="Order in which to apply preprocessing/filtering to the edx data."
                                "Names of filters and corresponding prameters should be specified as follows:"
                                "'filter_name par=val [par=val ...] [filter_name par=val ...]'. "
                                "valid filter_names are: {} |\n"
                                "valid paramaters are: {})".format(", ".join(filtering.FILTERS.keys()),
                                                                   str(filtering.FILTER_PARAMETERS)))
    prep_args.add_argument("--scale", action="store_true",
                           help="Flag indicating that element/edx data should be scaled(z-scores)"
                                "after preprocessing. ")
    prep_args.add_argument("--save_prep", required=False, choices=("csv", "image"), nargs="*",
                           help="If and how to save the prepped images. 'image' will result in separate images,"
                                "'csv' will result in a csv in which each row describes the chemical composition of a"
                                "single pixel.")
    # arguments regarding kmeans clustering of pixels based on chemical composition
    cluster_args = parser.add_argument_group("clustering options")
    cluster_args.add_argument("--n_start", type=int, default=15,
                              help="Number of iterations to do for a single K-means clustering.")
    cluster_args.add_argument("--cluster_threshold", type=float_range(0, 1), default=0.5,
                              help=("\n"
                                    "Minimum value of \"between_ss / total_ss\" the clustering should have.\n"
                                    "(sum of squared distances from each centroid to the sample mean,\n"
                                    "squared distances weighted by corresponding number of data points)\n"
                                    "divided by\n"
                                    "(sum of squared distances from each data point to the sample mean).\n"
                                    "This measure describes what proportion of the total variance is \n"
                                    "described/caught by the clustering.\n"
                                    "1 -> clustering perfectly  explains all variance.\n"
                                    "0 -> clustering explains no variance."))

    cluster_args.add_argument("--save_clustering", required=False, choices=("csv", "image"), nargs="*",
                              help="If and how to save pixel clustering results. see help of '--save_prep'")
    # argument regarding "blob extraction" from clustering image
    extraction_args = parser.add_argument_group("Blob extraction arguments")
    extraction_args.add_argument("--min_blob_size", type=int, default=5, help="Minimum size of pixel groups to extract "
                                                                              "after pixel clustering.")

    # arguments regarding glcm and texture measures
    glcm_args = parser.add_argument_group("GLCM arguments",
                                          "Arguments for computation of greylevel co-occurrence matrix. "
                                          "Please see http://hdl.handle.net/1880/51900")
    glcm_args.add_argument("-d", "--glcm_distance", type=int, default=2,
                           help="Dstance to use in pixel relation for glcm.")
    glcm_args.add_argument("-a", "--glcm_angle", type=float, default=0,
                           help="Angle(radians) to use in pixel relation for glcm")
    glcm_args.add_argument("-s", "--symmetric_glcm", action="store_true",
                           help="Weather to use a symmetric glcm or not.")
    glcm_args.add_argument("-n", "--normalize_glcm", action="store_true",
                           help="Weather to use a normalized glcm or not.")
    glcm_args.add_argument("--texture_measures", type=str, choices=glcm_calc.TEXTURE_MEASURES,
                           nargs="+", default=glcm_calc.TEXTURE_MEASURES,
                           help="Names of texture measures to calculate.")
    glcm_args.add_argument("--save_glcm_stats", action="store_true",
                           help="weather to save the computed texture measures or not.")

    return parser.parse_args()


def stack_and_save_images(flattened_images, filenames, im_shape):
    """
        saves all flattened images to a separate file
        :arg flattened_images 2-d np array in which every column represent an image.
        :arg filenames filenames to write images to
        :arg im_shape required shape for images
    """
    for image, filename in zip(flattened_images.T, filenames):
        imwrite(filename, image.reshape(im_shape), format="png")
    pass


def main(argv=None):
    if argv is None:
        argv = parse_args()
        print(argv)
    # work
    # check that outdir is safe to write to
    if not os.path.exists(argv.outdir):
        os.makedirs(argv.outdir)
    elif "n" in input("outdir already exists: {}. continue? y/n".format(argv.outdir)).lower():
        sys.exit("aborted program.")

    print("continuing..")
    os.chdir(argv.outdir)

    # ToDo add verbosity to main depending of argv.verbose
    # ### filter / preprocess edx data ###
    filtering_instructions = argv.order
    # all prepped images are flattened; each column of 2-d array represents ome element image
    prepped_images, colnames, image_size = filtering.preprocess_edx_data(argv.files,
                                                                         filtering_instructions,
                                                                         scale=argv.scale)
    used_filters = str(filtering_instructions).replace(" ", "_").replace(":", "")
    if argv.save_prep:
        # save preprocessed edx data to image and/or csv
        if "image" in argv.save_prep:
            filenames = ["{}_filtered({}).png".format(element, used_filters) for element in colnames]
            stack_and_save_images(prepped_images, filenames, image_size, )
        if "csv" in argv.save_prep:
            np.savetxt(used_filters + ".csv", prepped_images, header=" ".join(colnames))

    # ### cluster pixels ###
    labels, centers, score = auto_kmeans(prepped_images, argv.cluster_threshold,
                                         iter=argv.n_start, verbose=argv.verbose)
    label_image = labels.reshape(*image_size)
    if argv.save_clustering:
        if "image" in argv.save_clustering:
            imwrite("clustering_{}.png".format(used_filters), label_image)
        if "csv" in argv.save_clustering:
            np.savetxt("clustering{}.csv".format(used_filters), label_image, fmt="%i")

    # ### extract "blobs" from cluster_image ###
    em_image = imread(argv.image)[:, :, 0]

    blobs_per_group = object_extraction.get_objects(em_image, label_image, minsize=argv.min_blob_size)
    stats_per_group = glcm_calc.calculate_texture_measures(blobs_per_group, argv.texture_measures,
                                                           argv.glcm_distance, argv.glcm_angle,
                                                           symmetric=argv.symmetric_glcm,
                                                           normed=argv.normalize_glcm)
    if argv.save_glcm_stats:
        for group, stats in stats_per_group.items():
            np.savetxt("texture_measures_group_{}.csv".format(group), stats,
                       header=" ".join(argv.texture_measures))
    return


if __name__ == "__main__":
    sys.exit(main())
