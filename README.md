![XEMCLA logo](Logo.png)

Repository for the XEMCLA *(Energy-dispersive **X**-ray **E**lectron **M**icroscopy **Cl**ustering **A**nalysis)* project, aiming to cluster cell structures in EM images with extra atomic-element xray diffraction matrices/images. A more in depth description about the project can be found in our [preliminary report](https://bitbucket.org/twidlytkintertwerps/xemcla/downloads/preliminary_report_Casper_Johan.pdf) and [regular report](https://bitbucket.org/twidlytkintertwerps/xemcla/downloads/XEMCLA%20-%20Energy-dispersive%20X-ray%20Electron%20Microscopy%20Clustering%20Analysis%20-%20Casper%20Peters%20(341942)%20and%20Johan%20Schneiders%20(346312).pdf).

**Current version (GUI);** *v1.0*

**Current version (CLI);** *v0.9*

# EXAMPLE DATA;

Example data to test the program with can be found [here](https://bitbucket.org/twidlytkintertwerps/xemcla/downloads/Examples.zip).

# AUTHORS

XEMCLA is made by;

Bitbucket users: @Berghopper (Casper Peters) and @JohanSchneiders (Johan Schneiders)

# LICENSE

Copyright (c) [2018] [Casper Peters and Johan Schneiders] under the MIT license.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

# Notice for other developers who would like to use our code

To make it more quick and easy to use our code, [the latest src](https://bitbucket.org/twidlytkintertwerps/xemcla/downloads/src_v1.0_commit_e1f51ec53ffbd2fb8885c4544b5f4c87eb456d49_master.zip) is available in our bitbucket downloads. This might be handy if you do not want to clone the whole repository as it is quite large (~349.3 MB).

# **Table of contents**

* [1. Running with python](#markdown-header-1-running-with-python)
	* [1.1 Installing Dependencies](#markdown-header-11-installing-dependencies)
	* [1.2 Starting the gui](#markdown-header-12-starting-the-gui)
	* [1.3 Using the command-line alternative](#markdown-header-13-using-the-command-line-alternative)
* [2. Running without python](#markdown-header-2-running-without-python)
    * [2.1 Running with windows](#markdown-header-21-running-with-windows)
    * [2.2 Mac and linux users](#markdown-header-22-mac-and-linux-users)
* [3. GUI usage](#markdown-header-3-gui-usage)
* [4. How to build an executable for the GUI (using pyinstaller)](#markdown-header-4-how-to-build-an-executable-for-the-gui-using-pyinstaller)
* [5. XEMCLA Package and sub-packages information](#markdown-header-5-xemcla-package-and-sub-packages-information)

## 1. Running with python

### 1.1 Installing Dependencies

The following pip dependencies are needed (under python 3 and up);

- six
- PyWavelets
- toolz
- dask
- cloudpickle
- decorator
- networkx
- kiwisolver
- python-dateutil
- cycler
- pyparsing
- pytz
- numpy
- pillow
- imageio
- scikit-image
- scipy
- matplotlib
- tornado

These can be installed with the following command;

`pip install six PyWavelets toolz dask cloudpickle decorator networkx kiwisolver python-dateutil cycler pyparsing pytz numpy pillow imageio scikit-image scipy matplotlib tornado --upgrade`

\* *All standard python libraries are also needed, these for example include tkinter support.*

Both the GUI and CLI reside in the src folder of this project, to run these, you need to clone our repository or [download the latest src](https://bitbucket.org/twidlytkintertwerps/xemcla/downloads/src_v1.0_commit_e1f51ec53ffbd2fb8885c4544b5f4c87eb456d49_master.zip);

`git clone https://bitbucket.org/twidlytkintertwerps/xemcla.git`

The programs have been tested with the following versions of packages;
```
(venv) 11:17:16|ccpeters@bin310:~/Downloads/src$ pip freeze --local
cloudpickle==0.5.3
cycler==0.10.0
dask==0.18.0
decorator==4.3.0
imageio==2.3.0
kiwisolver==1.0.1
matplotlib==2.2.2
networkx==2.1
numpy==1.14.5
Pillow==5.1.0
pkg-resources==0.0.0
pyparsing==2.2.0
python-dateutil==2.7.3
pytz==2018.4
PyWavelets==0.5.2
scikit-image==0.14.0
scipy==1.1.0
six==1.11.0
toolz==0.9.0
tornado==5.0.2
(venv) 11:17:24|ccpeters@bin310:~/Downloads/src$ python --version
Python 3.5.3
(venv) 11:17:32|ccpeters@bin310:~/Downloads/src$
```

### 1.2 Starting the gui

Make sure you're using python3!

`xemcla/src$ ./xemcla_gui.py`

\* *It is recommended to launch the GUI in the terminal for useful messages and information while calculations are running.*

### 1.3 Using the command-line alternative

Make sure that, here as well, you use python3.

Example usage (unix);

```
xemcla/src$ export em_image="../Examples/data_rat_pancreas/data/Electron Image 18.png"
xemcla/src$ export edx_data=../Examples/data_rat_pancreas/data/*.csv
xemcla/src$ ./xemcla_cli.py  -i "$em_image" -f $edx_data --outdir ./output -o "tv weight=0.8" --scale --save_prep image csv --save_clustering image csv --verbose
```

In the above example, the `data_rat_pancreas` example is used. The `em_image` variable points to the Electron microscope image, `edx_data` points to the csv files. The edx data is first filtered with the total variance algorithm using a weight of 0.8, the data is also withen-scaled. After preprocessing a basic auto-kmeans done to cluster over the image. This is not explicitly specified, but does not need to be, the auto-kmeans threshold has a default of 0.5.

Full command-line help;

```
xemcla/src$ ./xemcla_cli.py -h
usage: xemcla_cli.py [-h] [--verbose] -i IMAGE -f FILES [FILES ...]
                     [--outdir OUTDIR] [-o [ORDER [ORDER ...]]] [--scale]
                     [--save_prep [{csv,image} [{csv,image} ...]]]
                     [--n_start N_START]
                     [--cluster_threshold CLUSTER_THRESHOLD]
                     [--save_clustering [{csv,image} [{csv,image} ...]]]
                     [--min_blob_size MIN_BLOB_SIZE] [-d GLCM_DISTANCE]
                     [-a GLCM_ANGLE] [-s] [-n]
                     [--texture_measures {contrast,dissimilarity,homogeneity,energy,correlation,ASM,mean_intensity} [{contrast,dissimilarity,homogeneity,energy,correlation,ASM,mean_intensity} ...]]
                     [--save_glcm_stats]

Takes an EM image and corresponding EDX data and performs the following
actions: -preprocesses the element data(csv's) -clusters the pixels of the
image based on chemical composition(preprocessed element data) -generates an
image, showing the clusterig -extracts "blobs"(group of contiguous pixels with
the same cluster label) -calculates grayscale cooccurance matrix and
corresponding statistics for every blob.

optional arguments:
  -h, --help            show this help message and exit
  --verbose             Flag indicating that extra progress information should
                        be printed.

input options:
  -i IMAGE, --image IMAGE
                        Path to original EM image.
  -f FILES [FILES ...], --files FILES [FILES ...]
                        Paths to csv files containing edx data.
  --outdir OUTDIR       Directory in which to place all output files. Will be
                        created if doesn't exist.

preprocessing arguments:
  -o [ORDER [ORDER ...]], --order [ORDER [ORDER ...]]
                        Order in which to apply preprocessing/filtering to the
                        edx data.Names of filters and corresponding prameters
                        should be specified as follows:'filter_name par=val
                        [par=val ...] [filter_name par=val ...]'. valid
                        filter_names are: tv, gaus, bilateral, non-
                        local_means, wavelet | valid paramaters are: {'tv':
                        'weight', 'gaus': 'sigma', 'bilateral':
                        'sigma_spatial', 'non-local_means': 'h', 'wavelet':
                        'sigma'})
  --scale               Flag indicating that element/edx data should be
                        scaled(z-scores)after preprocessing.
  --save_prep [{csv,image} [{csv,image} ...]]
                        If and how to save the prepped images. 'image' will
                        result in separate images,'csv' will result in a csv
                        in which each row describes the chemical composition
                        of asingle pixel.

clustering options:
  --n_start N_START     Number of iterations to do for a single K-means
                        clustering.
  --cluster_threshold CLUSTER_THRESHOLD
                        Minimum value of "between_ss / total_ss" the
                        clustering should have. (sum of squared distances from
                        each centroid to the sample mean, squared distances
                        weighted by corresponding number of data points)
                        divided by (sum of squared distances from each data
                        point to the sample mean). This measure describes what
                        proportion of the total variance is described/caught
                        by the clustering. 1 -> clustering perfectly explains
                        all variance. 0 -> clustering explains no variance.
  --save_clustering [{csv,image} [{csv,image} ...]]
                        If and how to save pixel clustering results. see help
                        of '--save_prep'

Blob extraction arguments:
  --min_blob_size MIN_BLOB_SIZE
                        Minimum size of pixel groups to extract after pixel
                        clustering.

GLCM arguments:
  Arguments for computation of greylevel co-occurrence matrix. Please see
  http://hdl.handle.net/1880/51900

  -d GLCM_DISTANCE, --glcm_distance GLCM_DISTANCE
                        Dstance to use in pixel relation for glcm.
  -a GLCM_ANGLE, --glcm_angle GLCM_ANGLE
                        Angle(radians) to use in pixel relation for glcm
  -s, --symmetric_glcm  Weather to use a symmetric glcm or not.
  -n, --normalize_glcm  Weather to use a normalized glcm or not.
  --texture_measures {contrast,dissimilarity,homogeneity,energy,correlation,ASM,mean_intensity} [{contrast,dissimilarity,homogeneity,energy,correlation,ASM,mean_intensity} ...]
                        Names of texture measures to calculate.
  --save_glcm_stats     weather to save the computed texture measures or not.
```

NOTE THAT THE COMMAND-LINE ALTERNATIVE IS LIMITED AND CANNOT DO THE FOLLOWING;

- Color in cluster images in RGB channels
- Do kmeans clustering with a specific K
- Clustering on texture features using blob extraction

## 2. Running without python

Raw binary executables will only come in the form of a graphical user interface or gui, thus for the command-line alternative, python is still needed.

### 2.1 Running with windows

Windows users can download the executable listed on this repository and just run it right away;
(Only win10 has been tried and tested.)

- [64-bit version v1.0](https://bitbucket.org/twidlytkintertwerps/xemcla/downloads/xemcla_gui_win64bit_v1.0.exe)

- [32-bit version v1.0](https://bitbucket.org/twidlytkintertwerps/xemcla/downloads/xemcla_gui_win32bit_v1.0.exe)

### 2.2 Mac and linux users

Mac and linux users are not supported in terms of executables with this project, and thus need to install python with all its dependencies to run it. It is also possible to compile your own executable using pyinstaller (see [chapter 4](#markdown-header-4-how-to-build-an-executable-for-the-gui-using-pyinstaller)).

## 3. GUI usage

The GUI will not be documented explicitely on this repo or in this readme, instead, a video tutorial is available;

https://www.youtube.com/watch?v=xvPZayL-xg8


## 4. How to build an executable for the GUI (using pyinstaller)
 
- use the latest pyinstaller dev version (standard version has been known to throw missing DLL errors);

`pip install https://github.com/pyinstaller/pyinstaller/archive/develop.zip`

- clone the repository or download [the latest src](https://bitbucket.org/twidlytkintertwerps/xemcla/downloads/src_v1.0_commit_e1f51ec53ffbd2fb8885c4544b5f4c87eb456d49_master.zip) (recommend to download src as the repository is quite big);

`git clone https://bitbucket.org/twidlytkintertwerps/xemcla.git`

- navigate to the source folder;

`cd xemcla/src/`

- before building, edit your respective `.spec` file (`xemcla_gui_windows_buid.spec`, `xemcla_gui_unix_build.spec` for windows and unix respectively) by modifying the `pathex` variable. This needs to point at your xemcla `src` folder.

- now build the executable;

`pyinstaller -F xemcla_gui_unix_build.spec`

or;

`pyinstaller -F xemcla_gui_windows_build.spec`

- If done correctly, a new `build` and `dist` folder will appear, your executable will reside in the `dist` folder.

\* *For building under windows, the Microsoft Visual C++ 2010 Redistributable Package might be needed.*

\* *Also note that for unix, it is still recommended to launch the application from the terminal, as it will print out quite useful information when running it.*

## 5. XEMCLA Package and sub-packages information

The xemcla project uses its own package and sub-packages, these are needed by both the command-line program and the gui. The package structure is listed below together with descriptions if applicable;

* xemcla_cli.py (Command line version of our project.)
* xemcl_gui.py (GUI version of our program, recommened, has more features.)
* xemcla_resources (General packages and resources for the xemcla project.)
    * image_processing (Package for image processing procedures.)
    * io (Input-Output modules.)
    * statistics (Package that holds all statistics related modules/libraries.)
    * widgets (General widgets and packages for the GUI interface.)
        * misc (Miscellaneous modules, handy for some tk functionality.)
        * moarwidgets (Tkinter generally is an implicit package, this package aims to have widgets mora explicitly defined.)
        * tabs (Package for all tab-modules used in the xemcla GUI.)

\* **For full documentation of each script and module, it is recommended to download [the latest zipped src](https://bitbucket.org/twidlytkintertwerps/xemcla/downloads/src_v1.0_commit_e1f51ec53ffbd2fb8885c4544b5f4c87eb456d49_master.zip).**

